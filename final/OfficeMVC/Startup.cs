﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OfficeMVC.Startup))]
namespace OfficeMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
