﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DAL
{
    public class BidDB
    {
        public static async Task<bool> createBid(BLL.Bid bid, string groupId)
        {
            Model m = new Model();
            ParseObject obj = await m.retrieveMyBusiness();
            bid.businessId = obj.ObjectId;


            await m.createBid(bid, groupId);
            return true;
        }


        public static async Task<BLL.Bid> BidDetails(string bidID)
        {
            Model m = new Model();
            ParseObject bidParse = await m.retrieveBidByID(bidID);
            BLL.Bid bid = ConvertFromParseObject.convertParseToBid(bidParse, null);
            return bid;
        }


        public static async System.Threading.Tasks.Task<bool> selectWinningBid(string groupId)
        {
            Model model = new Model();
            IEnumerable<ParseObject> bids = await model.retrieveBids(groupId);
            //if there are no bids - dont open a group
            if (bids == null || bids.Count()==0)
            {
               await model.deleteGroup(groupId);
                return false;
            }
            double maxScore = 0;
            ParseObject bestBid = null;

            double stepDifference = 0;
            double lowestPrice = 0;
            double startingPrice = 0;
            double deliveryCapability = 0;
            double shippingCapability = 0;
            double guaranteeCapability = 0;

            foreach (ParseObject bid in bids)
            {
                //step difference calculation
                double step2to3Difference = model.getBidPriceStep2(bid) - model.getBidPriceStep3(bid);
                double step3to4Difference = model.getBidPriceStep3(bid) - model.getBidPriceStep4(bid);
                if (step2to3Difference > step3to4Difference)
                    stepDifference = step2to3Difference * Constants.CALCULATION_STEPS_DIFFERENCE / 100;
                else
                    stepDifference = step3to4Difference * Constants.CALCULATION_STEPS_DIFFERENCE / 100;

                //lowest price calculation
                //not always there is a 5th step. if exsist - take it.
                if (bid.Get<double>(Constants.PRICE_STEP_5) != 0)
                    lowestPrice = model.getBidPriceStep5(bid) * Constants.CALCULATION_LOWEST_PRICE / 100;
                else
                    lowestPrice = model.getBidPriceStep4(bid) * Constants.CALCULATION_LOWEST_PRICE / 100;

                //starting price calculation
                startingPrice = model.getBidOriginalPrice(bid) * Constants.CALCULATION_STARTING_PRICE / 100;

                //delivery capability calculation
                deliveryCapability = model.getBidMaxUints(bid) * Constants.CALCULATION_DELIVERY_CAPABILITY / 100;

                //shipping capability calculation
                if (model.getBidShipping(bid) == true)
                    shippingCapability = Constants.CALCULATION_SHIPPING_CAPABILITY;
                else
                    shippingCapability = 0;

                //guarantee capability calculation
                guaranteeCapability = model.getBidGuarantee(bid) * Constants.CALCULATION_GUARANTEE_CAPABILITY / 100;

                //add review if necessary


                double score = stepDifference + lowestPrice + startingPrice + deliveryCapability + shippingCapability + guaranteeCapability;
                if (score > maxScore)
                {
                    maxScore = score;
                    bestBid = bid;
                }
            }

            try
            {
                await model.createWinningBid(bestBid, groupId);
                return true;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }
    }
}