﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Parse;

namespace DAL
{
    public class ProductDB
    {
        public static async System.Threading.Tasks.Task<List<BLL.Product>> getAllProducts(bool isBusiness)
        {
            Model model = new Model();
            IEnumerable<ParseObject> allProducts = null;

            if (isBusiness)
            {
                ParseObject myBusiness = await model.retrieveMyBusiness();
                allProducts = await model.retrieveAllMyPoducts(myBusiness.ObjectId);

            }
            else
            {
                allProducts = await model.retrieveAllProducts();
            }

            List<BLL.Product> products = new List<BLL.Product>();
            foreach (ParseObject parseProduct in allProducts)
            {
                BLL.Product product = ConvertFromParseObject.ConvertParseToProduct(parseProduct, null, null, null);
                products.Add(product);
            }
            products.OrderBy(x => x.title);
            return products;
        }


        public static async System.Threading.Tasks.Task<List<BLL.ProductCategory>> getAllProductCategories()
        {
            Model model = new Model();
            List<BLL.ProductCategory> productCategories = new List<BLL.ProductCategory>();
            IEnumerable<ParseObject> categories = await model.retrieveCategories();
            foreach (ParseObject parseCategory in categories)
            {
                BLL.ProductCategory category = ConvertFromParseObject.convertParseToProductCategory(parseCategory);
                productCategories.Add(category);
            }
            return productCategories;
        }

        public static async System.Threading.Tasks.Task<List<BLL.ProductSubCategory>> getAllProductSubCategories()
        {
            Model model = new Model();
            List<BLL.ProductSubCategory> productSubCategories = new List<BLL.ProductSubCategory>();
            IEnumerable<ParseObject> subCategories = await model.retrieveSubCategories();
            foreach (ParseObject parseSubCategory in subCategories)
            {
                BLL.ProductSubCategory subCategory = ConvertFromParseObject.convertParseToProductSubCategory(parseSubCategory);
                productSubCategories.Add(subCategory);
            }
            return productSubCategories;
        }

        public static async System.Threading.Tasks.Task<BLL.Product> LoadProduct(string productId)
        {
            Model model = new Model();
            ParseObject productParse = await model.retrieveProduct(productId);
            BLL.Product product = ConvertFromParseObject.ConvertParseToProduct(productParse, null, null, null);
            if (product.productTypeName.Equals(Constants.PRODUCT_CELLPHONE))
            {
                ParseObject productType = await model.retrieveProductDetails(productParse, product.productTypeName);
                product = ConvertFromParseObject.convertParseProductToProductType(product, productType);
            }
            return product;
        }







        // ---- For Ajax calls ---- //
        public static async System.Threading.Tasks.Task<List<BLL.ProductSubCategory>> LoadSubCategories(string categoryId)
        {
            Model model = new Model();
            List<BLL.ProductSubCategory> sub_Categories = new List<BLL.ProductSubCategory>();
            IEnumerable<ParseObject> subCategories = await model.retrieveSubCategoriesOfCategory(categoryId);
            foreach (ParseObject parseSubCategory in subCategories)
            {
                    BLL.ProductSubCategory subCategory = ConvertFromParseObject.convertParseToProductSubCategory(parseSubCategory);
                    sub_Categories.Add(subCategory);
         
            }
            return sub_Categories;
        }


        public static async System.Threading.Tasks.Task<List<BLL.Product>> LoadProducts(string subCategoryId)
        {

            Model model = new Model();
            List<BLL.Product> products = new List<BLL.Product>();
            IEnumerable<ParseObject> parse_products = await model.retrieveProductsOfSubCategory(subCategoryId);
            foreach (ParseObject parseProduct in parse_products)
            {
                BLL.Product product = ConvertFromParseObject.ConvertParseToProduct(parseProduct, null, null, null);
                products.Add(product);
            }
            return products;
        }

        public static async System.Threading.Tasks.Task<List<BLL.Product>> SearchAutocomplete(string productName)
        {
            Model model = new Model();
            List<BLL.Product> productsList = new List<BLL.Product>();
            IEnumerable<ParseObject> products = await model.retrieveAllProducts();
            foreach (ParseObject parseProduct in products)
            {
                if (parseProduct.Get<string>(Constants.TITLE).StartsWith(productName, true, new System.Globalization.CultureInfo("en-US")))
                {
                    BLL.Product product = ConvertFromParseObject.ConvertParseToProduct(parseProduct, null, null, null);
                    productsList.Add(product);
                }
            }
            return productsList;
        }


    }
}