﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL
{
    public class BusinessDashboardDB
    {
        //check if user is business or not
        public static bool userIsBusiness()
        {
            Model model = new Model();
            ParseUser user = ParseUser.CurrentUser;
            return model.isBusiness(user);
        }

        public static async System.Threading.Tasks.Task<BLL.Business> loadBusiness()
        {
            Model model = new Model();
            ParseObject parseBusiness = await model.retrieveMyBusiness();
            IEnumerable<ParseObject> parseProductsBusiness = await model.retrieveAllMyPoducts(parseBusiness.ObjectId);
            ParseFile businessLogo = model.retrieveLogoOfBusiness(parseBusiness);

            BLL.Business business = ConvertFromParseObject.convertParseToBusiness(parseBusiness, parseProductsBusiness, businessLogo);
            return business;
        }

        

        public static async System.Threading.Tasks.Task<List<BLL.GroupBuying>> loadGroupsThatBusinessWon(string businessID)
        {
            Model model = new Model();
            IEnumerable<ParseObject> activeGroups = await model.retrieveGroupsBusinessWon(businessID); //groups that business won
            List<BLL.GroupBuying> activeGroupsForBusiness = new List<BLL.GroupBuying>();
            foreach (ParseObject a in activeGroups)
            {
                ParseObject product = await model.retrieveProductOfGroup(a);
                IEnumerable<ParseObject> usersOfGroup = await model.retrieveUsersOfGroup(a);
                BLL.GroupBuying active_group = ConvertFromParseObject.convertParseToGroupBuying(a, product, null, null, null, null, usersOfGroup, null, null);
                ParseObject winningBid = await model.retrieveWinningBid(a);
                active_group.winningBid.bidID = winningBid.ObjectId;
                activeGroupsForBusiness.Add(active_group);
            }
            return activeGroupsForBusiness;
        }



        public static async System.Threading.Tasks.Task<List<BLL.GroupBuying>> loadGroupsWaitingForBids(string businessID)
        {
            Model model = new Model();
            List<ParseObject> groups = new List<ParseObject>();
            List<ParseObject> products = new List<ParseObject>();
            IEnumerable<ParseObject> allGroups = await model.retrieveAllActiveGroupsWithoutWinningBid();
            IEnumerable<ParseObject> allMyProductsParse = await model.retrieveAllMyPoducts(businessID);
            foreach (ParseObject group in allGroups)
            {
                ParseObject product = await model.retrieveProductOfGroup(group);
                //check if my business has this particular product
                foreach (ParseObject p in allMyProductsParse)
                {
                    if (p.ObjectId == product.ObjectId)
                    {
                            products.Add(product);
                            groups.Add(group);
                    }
                }
            }

            List<BLL.GroupBuying> groupsWaitingForBids = new List<BLL.GroupBuying>();
            int i = 0;
            foreach (ParseObject g in groups)
            {
                BLL.GroupBuying group = ConvertFromParseObject.convertParseToGroupBuying(g, products[i], null, null, null, null, null, null, null);
                groupsWaitingForBids.Add(group);
                i++;
            }
            return groupsWaitingForBids;
        }


        //check if the difference between NOW and creation time of the group is under 24 horus
        private static TimeSpan canBusinessOferBid(DateTime groupCreationTime)
        {
            TimeSpan ts = DateTime.Now - groupCreationTime;
            int differenceInDays = ts.Days;
            int differenceInHours = ts.Hours;
            if (differenceInDays == 0 && differenceInHours < 24)
                return ts;
            //if not
            return TimeSpan.Zero;
        }




        public static async System.Threading.Tasks.Task<List<BLL.Product>> loadAllProducts(string businessId)
        {
            Model model = new Model();
            IEnumerable<ParseObject> productsParse = await model.retrieveAllMyPoducts(businessId);
            List<BLL.Product> products = new List<BLL.Product>();
            foreach(ParseObject p in productsParse)
            {
                ParseFile productImage = model.getProductImage(p);
                BLL.Product tempProduct = ConvertFromParseObject.ConvertParseToProduct(p, productImage, null, null);
                products.Add(tempProduct);
            }
            return products;
        }

        public static async System.Threading.Tasks.Task<bool> SaveBusiness(BLL.Business business)
        {
            Model model = new Model();
            return await model.SaveBusiness(business);   
        }

        public static async System.Threading.Tasks.Task<bool> UpdateBusinessSettings(BLL.Business business)
        {
            Model model = new Model();
            return await model.SaveBusiness(business);
        }


        public static async System.Threading.Tasks.Task<BLL.Business> loadBusinessByID(string businessId)
        {
            Model model = new Model();
            ParseObject business = await model.retrieveBusiness(businessId);
            ParseFile bLogo = model.retrieveLogoOfBusiness(business);
            IEnumerable<ParseObject> productsOfbusiness = await model.retrieveAllMyPoducts(business.ObjectId);
            BLL.Business b = ConvertFromParseObject.convertParseToBusiness(business, productsOfbusiness, bLogo);
            return b;
        }





    }
}