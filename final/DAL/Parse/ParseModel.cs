﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Parse;
using System.Threading.Tasks;
using BLL;

namespace DAL
{
    public class ParseModel : ParseInterface
    {

        public Task<bool> deleteGroup(string groupId)
        {
            return GroupParse.deleteGroup(groupId);
        }
        //business
        public Task<List<string>> getMailOfBusinessesWhoHaveThisProducts(string productId)
        {
            return BusinessParse.getMailOfBusinessesWhoHaveThisProducts(productId);
        }
        public Task<ParseObject> retrieveMyBusiness()
        {
            return BusinessParse.retrieveMyBusiness();
        }

        public Task<ParseObject> retrieveBusiness(string businessId)
        {
            return BusinessParse.retrieveBusiness(businessId);
        }

        public Task<bool> addProductToBusiness(string productId, ParseObject business)
        {
            return BusinessParse.addProductToBusiness(productId, business);
        }

        public Task<bool> removeProductFromBusiness(string productId)
        {
            return BusinessParse.removeProductFromBusiness(productId);
        }


        public Task<IEnumerable<ParseObject>> retrieveAllActiveGroupsForBusiness(string businessId)
        {
            return BusinessParse.retrieveAllActiveGroupsForBusiness(businessId);
        }

        public ParseFile retrieveLogoOfBusiness(ParseObject business)
        {
            return BusinessParse.retrieveLogoOfBusiness(business);
        }

        public Task<string> retrieveEmailFromBusiness(ParseObject business)
        {
            return BusinessParse.retrieveEmailFromBusiness(business);
        }
        public Task<IEnumerable<ParseObject>> retrieveGroupsBusinessWon(string businessId)
        {
            return BusinessParse.retrieveGroupsBusinessWon(businessId);
        }
        

        //product
        public Task<IEnumerable<ParseObject>> businessesWhoHaveThisProduct(ParseObject product)
        {
            return ProductParse.businessesWhoHaveThisProduct(product);
        }
        public Task<IEnumerable<ParseObject>> retrieveAllProducts()
        {
            return ProductParse.retrieveAllProducts();
        }
        public Task<ParseObject> retrieveProduct(string productID)
        {
            return ProductParse.retrieveProduct(productID);
        }
        public Task<IEnumerable<ParseObject>> retrieveAllMyPoducts(string businessId)
        {
            return ProductParse.retrieveAllMyPoducts(businessId);
        }
        public Task<ParseObject> retrieveActiveGroupForProduct(string productID)
        {
            return ProductParse.retrieveActiveGroupForProduct(productID);
        }

        public string getProductTitle(ParseObject product)
        {
            return ProductParse.getProductTitle(product);
        }
        public ParseFile getProductImage(ParseObject product)
        {
            return ProductParse.getProductImage(product);
        }
        public Task<bool> addBusinessToProduct(string productId, ParseObject business)
        {
            return ProductParse.addBusinessToProduct(productId, business);

        }
        public Task<bool> removeBusinessFromProduct(string productId, ParseObject business)
        {
            return ProductParse.removeBusinessFromProduct(productId, business);
        }

        public Task<IEnumerable<ParseObject>> retrieveProductsOfSubCategory(string subCategoryId)
        {
            return ProductParse.retrieveProductsOfSubCategory(subCategoryId);
        }

        public Task<ParseObject> retrieveProductDetails(ParseObject parseProductID, string productTypeName)
        {
            return ProductParse.retrieveProductDetails(parseProductID, productTypeName);
        }

        public Task<ParseObject> retrieveManufacturer(ParseObject parseProduct)
        {
            return ProductParse.retrieveManufacturer(parseProduct);
        }
        public IEnumerable<ParseFile> getProductSliderImages(ParseObject parseProduct)
        {
            return ProductParse.getProductSliderImages(parseProduct);
        }



        //group
        public Task<string> retrieveUserPref(string userId)
        {
            return GroupParse.retrieveUserPref(userId);
        }

        public Task<IEnumerable<ParseObject>> retrieveAllActiveGroups()
        {
            return GroupParse.retrieveAllActiveGroups();
        }
        public Task<BLL.HomePageModel> retrieveGroupsForHomePage()
        {
            return GroupParse.retrieveGroupsForHomePage();
        }
        public Task<IEnumerable<ParseObject>> retrieveAllActiveGroupsWithoutWinningBid()
        {
            return GroupParse.retrieveAllActiveGroupsWithoutWinningBid();
        }
        public Task<string> createGroup(ParseObject product)
        {
            return GroupParse.createGroup(product);

        }

        public Task<IEnumerable<ParseUser>> retrieveUsersOfGroup(ParseObject group)
        {
            return GroupParse.retrieveUsersOfGroup(group);
        }

        public Task<ParseObject> retrieveProductOfGroup(ParseObject group)
        {
            return GroupParse.retrieveProductOfGroup(group);
        }

        public Task<ParseObject> retrieveGroup(string groupId)
        {
            return GroupParse.retrieveGroup(groupId);
        }

        public DateTime getExpirationDate(ParseObject group)
        {
            return GroupParse.getExpirationDate(group);
        }
        public Task<bool> setGroupActive(ParseObject group, bool status)
        {
            return GroupParse.setGroupActive(group, status);
        }

        public Task<bool> checkAllGroupsExpireDate()
        {
            return GroupParse.checkAllGroupsExpireDate();
        }

        public Task<bool> joinToGroup(ParseObject group)
        {
            return GroupParse.joinToGroup(group);
        }

        public Task<bool> deleteProductFromBusiness(string productId)
        {
            return GroupParse.deleteProductFromBusiness(productId);
        }



        //winning bid
        public Task<IEnumerable<ParseObject>> retrieveWinningBids()
        {
            return WinningBidParse.retrieveWinningBids();
        }

        public Task<ParseObject> retrieveBidOfWinningBid(ParseObject winningBid)
        {
            return WinningBidParse.retrieveBidOfWinningBid(winningBid);
        }

        public Task<bool> createWinningBid(ParseObject bid, string groupId)
        {
            return WinningBidParse.createWinningBid(bid, groupId);
        }

        public Task<ParseObject> retrieveWinningBid(ParseObject group)
        {
            return WinningBidParse.retrieveWinningBid(group);
        }

        public Task<ParseObject>  retrieveBusinessOfWinningBid(ParseObject winningBid)
        {
            return WinningBidParse.retrieveBusinessOfWinningBid(winningBid);
        }
        //bid
        public Task<ParseObject> retrieveGroupOfBid(ParseObject bid)
        {
            return BidParse.retrieveGroupOfBid(bid);
        }

        public Task<bool> createBid(BLL.Bid bid, string groupId)
        {
            return BidParse.createBid(bid, groupId);
        }
        public Task<IEnumerable<ParseObject>> retrieveBids(string groupId)
        {
            return BidParse.retrieveBids(groupId);
        }

        public Task<ParseObject> getBid(ParseObject winningBid)
        {
            return BidParse.getBid(winningBid);
        }


        public double getBidPriceStep1(ParseObject bid)
        {
            return BidParse.getBidPriceStep1(bid);
        }

        public double getBidPriceStep2(ParseObject bid)
        {
            return BidParse.getBidPriceStep2(bid);
        }

        public double getBidPriceStep3(ParseObject bid)
        {
            return BidParse.getBidPriceStep3(bid);
        }

        public double getBidPriceStep4(ParseObject bid)
        {
            return BidParse.getBidPriceStep4(bid);
        }

        public double getBidPriceStep5(ParseObject bid)
        {
            return BidParse.getBidPriceStep5(bid);
        }

        public double getBidGuarantee(ParseObject bid)
        {
            return BidParse.getBidGuarantee(bid);
        }

        public double getBidOriginalPrice(ParseObject bid)
        {
            return BidParse.getBidOriginalPrice(bid);
        }

        public double getBidMaxUints(ParseObject bid)
        {
            return BidParse.getBidMaxUints(bid);
        }

        public bool getBidShipping(ParseObject bid)
        {
            return BidParse.getBidShipping(bid);
        }

        public string getBusinessIdOfBid(ParseObject bid)
        {
            return BidParse.getBusinessIdOfBid(bid);
        }

        public string getCommentsOfBid(ParseObject bid)
        {
            return BidParse.getCommentsOfBid(bid);
        }

        public Task<ParseObject> retrieveBidByID(string bidId)
        {
            return BidParse.retrieveBidByID(bidId);
        }

        



        //category
        public Task<IEnumerable<ParseObject>> retrieveCategories()
        {
            return CategoryParse.retrieveCategories();
        }

        public string getCategoryName(ParseObject category)
        {
            return CategoryParse.getCategoryName(category);
        }

        //sub category
        public Task<IEnumerable<ParseObject>> retrieveSubCategories()
        {
            return SubCategoryParse.retrieveSubCategories();
        }

        public string getSubCategoryName(ParseObject subCategory)
        {
            return SubCategoryParse.getSubCategoryName(subCategory);
        }

        public Task<IEnumerable<ParseObject>> retrieveSubCategoriesOfCategory(string categoryId)
        {
            return SubCategoryParse.retrieveSubCategoriesOfCategory(categoryId);
        }

        //user
        public bool isBusiness(ParseUser user)
        {
            return UserParse.isBusiness(user);
        }

        public string getCurrentUserId()
        {
            return UserParse.getCurrentUserId();
        }

        public Task<string> getCurrentUserSession()
        {
            return UserParse.getCurrentUserSession();
        }

        public Task<IEnumerable<ParseObject>> retrieveActiveGroupsUserBelongTo()
        {
            return UserParse.retrieveActiveGroupsUserBelongTo();
        }

        public Task<bool> saveUserCreditCardId(string creditCardId)
        {
            return UserParse.saveUserCreditCardId(creditCardId);
        }

        public string getCreditCatdId()
        {
            return UserParse.getCreditCatdId();
        }

        public Task<bool> addUserToPref(string userID)
        {
            return UserParse.addUserToPref(userID);
        }





        public Task<bool> SaveBusiness(Business business)
        {
            return BusinessParse.SaveBusiness(business);
        }
    }
}