﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Parse;
using System.IO;

namespace DAL
{
    public class BusinessParse
    {

        public static async Task<List<string>> getMailOfBusinessesWhoHaveThisProducts(string productId)
        {
            try
            {
                Model m = new Model();
                ParseObject product = await m.retrieveProduct(productId);
                var query = ParseObject.GetQuery(Constants.BUSINESS_TABLE)
                    .WhereEqualTo(Constants.PRODUCTS, product).Include(Constants.USER);

                IEnumerable<ParseObject> businesses = await query.FindAsync();
                List<string> emails = new List<string>();
                foreach(ParseObject obj in businesses)
                {
                    emails.Add(obj.Get<ParseUser>(Constants.USER).Email);
                }
                return emails;
            }
            catch (Exception e)
            {
                //add log
                return null;
            }
        }


        public static async Task<ParseObject> retrieveMyBusiness()
        {
            try
            {
                var currentUser = ParseUser.CurrentUser;
                var myBusinessQuery = ParseObject.GetQuery(Constants.BUSINESS_TABLE).WhereEqualTo(Constants.USER, currentUser);
                ParseObject myBusiness = await myBusinessQuery.FirstOrDefaultAsync();
                return myBusiness;
            }
            catch (Exception e)
            {
                //add log
                return null;
            }
        }

        public static async Task<ParseObject> retrieveBusiness(string businessId)
        {
            try
            {
                var businessQuery = ParseObject.GetQuery(Constants.BUSINESS_TABLE).WhereEqualTo(Constants.OBJECT_ID, businessId);
                ParseObject business = await businessQuery.FirstAsync();
                return business;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<bool> addProductToBusiness(string productId, ParseObject business)
        {
            try
            {
                //get product
                Model model = new Model();
                ParseObject product = await model.retrieveProduct(productId);

                var relation = business.GetRelation<ParseObject>(Constants.PRODUCTS);
                relation.Add(product);
                await business.SaveAsync();

                return true; 
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<bool> removeProductFromBusiness(string productId)
        {
            try
            {
                //get product
                Model model = new Model();
                ParseObject business = await model.retrieveMyBusiness();
                ParseObject product = await model.retrieveProduct(productId);

                var relation = business.GetRelation<ParseObject>(Constants.PRODUCTS);
                relation.Remove(product);
                await business.SaveAsync();

                return true;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static ParseFile retrieveLogoOfBusiness(ParseObject business)
        {
            return business.Get<ParseFile>(Constants.LOGO);
        }

        public static async Task<IEnumerable<ParseObject>> retrieveAllActiveGroupsForBusiness(string businessId)
        {
            Model model = new Model();
            ParseObject business = await model.retrieveBusiness(businessId);
            IEnumerable<ParseObject> businessProducts = await model.retrieveAllMyPoducts(businessId); 
            IEnumerable<ParseObject> allActiveGroups = await model.retrieveAllActiveGroups();
            List<ParseObject> allActiveGroupsForBusiness = new List<ParseObject>();
            foreach (ParseObject s in allActiveGroups)
            {
                ParseObject winningBid = await model.retrieveWinningBid(s);
                string winner_business_id = model.getBusinessIdOfBid(winningBid);
                if(winner_business_id == businessId)
                {
                        allActiveGroupsForBusiness.Add(s);
                }
                else if (winningBid.ObjectId == "YGgEGROuTd")
                    continue;  
            }

            IEnumerable<ParseObject> temp = allActiveGroupsForBusiness;
            return temp;
        }





        public static async Task<IEnumerable<ParseObject>> retrieveGroupsBusinessWon(string businessId)
        {
            Model model = new Model();
            IEnumerable<ParseObject> allActiveGroups = await model.retrieveAllActiveGroups();
            List<ParseObject> allActiveGroupsForBusiness = new List<ParseObject>();
            foreach (ParseObject s in allActiveGroups)
            {
                ParseObject winningBid = await model.retrieveWinningBid(s);
                if(winningBid != null)
                {
                    ParseObject b = await model.retrieveBusinessOfWinningBid(winningBid);
                    if (b.ObjectId == businessId)
                    {
                        allActiveGroupsForBusiness.Add(s);
                    }
                }
                
            }
            return allActiveGroupsForBusiness;            
        }




        //for new business who created right now
        public static async Task<bool> SaveBusiness(BLL.Business business)
        {
            Model model = new Model();
            try
            {
                //update
                if (!business.businessID.Equals("0"))
                {
                    ParseObject business1 = await model.retrieveBusiness(business.businessID);
                    if (business.logo != null)
                    {
                        BinaryReader rdr = new BinaryReader(business.logo.InputStream);
                        byte[] data = rdr.ReadBytes((int)business.logo.ContentLength);
                        ParseFile file = new ParseFile(business.logo.FileName, data);
                        await file.SaveAsync();
                        business1[Constants.LOGO] = file;
                    }
                    business1[Constants.NAME] = business.businessName;
                    business1[Constants.ADRESS] = business.address;
                    business1[Constants.FACEBOOK_PAGE] = business.facebookPage;
                    business1[Constants.ABOUT_US] = business.aboutUs;
                    business1[Constants.WEBSITE] = business.website;
                    business1[Constants.PHONE_NUMBER] = business.phoneNumber;
                    business1[Constants.USER] = ParseUser.CurrentUser;
                    await business1.SaveAsync();
                }
                //save new business
                else
                {
                    var newBusiness = new ParseObject(Constants.BUSINESS_TABLE);
                    if (business.logo != null)
                    {
                        BinaryReader rdr = new BinaryReader(business.logo.InputStream);
                        byte[] data = rdr.ReadBytes((int)business.logo.ContentLength);
                        ParseFile file = new ParseFile(business.logo.FileName, data);
                        await file.SaveAsync();
                        newBusiness[Constants.LOGO] = file;
                    }
                    newBusiness[Constants.NAME] = business.businessName;
                    newBusiness[Constants.ADRESS] = business.address;
                    newBusiness[Constants.FACEBOOK_PAGE] = business.facebookPage;
                    newBusiness[Constants.ABOUT_US] = business.aboutUs;
                    newBusiness[Constants.WEBSITE] = business.website;
                    newBusiness[Constants.PHONE_NUMBER] = business.phoneNumber;
                    newBusiness[Constants.USER] = ParseUser.CurrentUser;
                    await newBusiness.SaveAsync();
                }
                return true;
            }
            catch (Exception e) { }
            return false;
        }


        public static async Task<string> retrieveEmailFromBusiness(ParseObject business)
        {
            try
            {
                Model model = new Model();
                IEnumerable<ParseUser> user = await ParseUser.Query
                    .WhereEqualTo(Constants.BUSINESS, business)
                    .FindAsync();

                ParseUser result = user.First<ParseUser>();
                return result.Email;
            }
            catch { return null; }
        }
    }
}