﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace DAL
{
    public class UserParse
    {
        public static bool isBusiness(ParseUser user)
        {
            return user.Get<bool>(Constants.IS_BUSINESS);
        }

        public static string getCurrentUserId()
        {
            return ParseUser.CurrentUser.ObjectId;
        }

        public static async Task<string> getCurrentUserSession()
        {
            ParseUser u = Parse.ParseUser.CurrentUser;


            try
            {
                var sessionQuery = ParseSession.GetQuery(Constants.SESSION).WhereEqualTo(Constants.USER, ParseUser.CurrentUser.ObjectId);
                ParseObject sessionParse = await sessionQuery.FirstAsync();


                string session = sessionParse.Get<string>(Constants.SESSION_TOKEN);
                return session;
            }
            catch(Exception e) { }
            return null;
        }



        public static async Task<IEnumerable<ParseObject>> retrieveActiveGroupsUserBelongTo()
        {
            Parse.ParseUser currentUser = ParseUser.CurrentUser;
            var groupsRelation = currentUser.GetRelation<ParseObject>(Constants.GROUP_BUYING);


            // to change this rows.....//
            Model model = new Model();
            IEnumerable<ParseObject> allActiveGroups = await model.retrieveAllActiveGroups();
            return allActiveGroups;
        }

        public static async Task<bool> saveUserCreditCardId(string creditCardId)
        {
            try
            {
                ParseUser.CurrentUser[Constants.CREDIT_CARD_ID] = creditCardId;
                await ParseUser.CurrentUser.SaveAsync();

            }
            catch
            {
                return false;
            }
            return true;
        }

        public static string getCreditCatdId()
        {
            return ParseUser.CurrentUser.Get<string>(Constants.CREDIT_CARD_ID);
        }


        public static async Task<bool> addUserToPref(string userID)
        {
            try
            {
                ParseObject pref = new ParseObject(Constants.PREF_TABLE);
                pref[Constants.PREF] = "null";
                pref[Constants.USER_ID] = userID;
                await pref.SaveAsync();   
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}