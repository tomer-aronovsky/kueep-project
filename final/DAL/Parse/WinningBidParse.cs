﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Parse;

namespace DAL
{
    public class WinningBidParse
    {
        public static async Task<IEnumerable<ParseObject>> retrieveWinningBids()
        {
            try
            {
                var winningBidsQuery = ParseObject.GetQuery(Constants.WINNING_BID_TABLE);
                IEnumerable<ParseObject> winningBids = await winningBidsQuery.FindAsync();
                return winningBids;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<ParseObject> retrieveBidOfWinningBid(ParseObject winningBid)
        {
            Model model = new Model();
            try
            {
                ParseObject bid = await model.getBid(winningBid);
                var bidQuery = ParseObject.GetQuery(Constants.BID_TABLE).WhereEqualTo(Constants.OBJECT_ID, bid.ObjectId);
                //ParseObject bid = await bidQuery.FirstAsync();

                return bid;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<bool> createWinningBid(ParseObject bid, string groupId)
        {
            try
            {
                Model m = new Model();
                ParseObject group = await m.retrieveGroup(groupId);
                group[Constants.WINNING_BID] = bid;


                await group.SaveAsync();
                return true;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }


        public static async Task<ParseObject> retrieveWinningBid(ParseObject group)
        {
            try
            {
                ParseObject bidFromGroup = group.Get<ParseObject>(Constants.WINNING_BID);
                var BidQuery = ParseObject.GetQuery(Constants.BID_TABLE).WhereEqualTo(Constants.OBJECT_ID, bidFromGroup.ObjectId);
                ParseObject bid = await BidQuery.FirstAsync();

                return bid;
            }
            catch (Exception e)
            {
                return null;
            }
        }




        public static async Task<ParseObject> retrieveBusinessOfWinningBid(ParseObject winningBid)
        {
            try
            {
                ParseObject businessFromWinnigBid = winningBid.Get<ParseObject>(Constants.BUSINESS);
                var BusinessQuery = ParseObject.GetQuery(Constants.BUSINESS_TABLE).WhereEqualTo(Constants.OBJECT_ID, businessFromWinnigBid.ObjectId);
                ParseObject business = await BusinessQuery.FirstAsync();

                return business;   
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}