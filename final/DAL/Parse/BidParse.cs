﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Parse;

namespace DAL
{
    public class BidParse
    {
        public static async Task<ParseObject> retrieveGroupOfBid(ParseObject bid)
        {
            try
            {
                //minGroup is just to obtain the group objectId
                ParseObject minGroup = bid.Get<ParseObject>(Constants.GROUP_BUYING);

                //group is a Pointer in the bid, therefore we need to query him
                var groupQuery = ParseObject.GetQuery(Constants.GROUP_BUYING_TABLE).WhereEqualTo(Constants.OBJECT_ID, minGroup.ObjectId);
                ParseObject group = await groupQuery.FirstAsync();

                return group;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<bool> createBid(BLL.Bid bid, string groupId)
        {
            try
            {
                var bidObject = new ParseObject(Constants.BID_TABLE);

                //TODO: check max units - between 10-50
                bidObject[Constants.MAX_UNITS] = bid.max_units;


                bidObject[Constants.ORIGINAL_PRICE] = bid.original_price;
                bidObject[Constants.PRICE_STEP_1] = bid.price_step_1;
                bidObject[Constants.PRICE_STEP_2] = bid.price_step_2;
                bidObject[Constants.PRICE_STEP_3] = bid.price_step_3;
                bidObject[Constants.PRICE_STEP_4] = bid.price_step_4;
                bidObject[Constants.PRICE_STEP_5] = bid.price_step_5;
                bidObject[Constants.COMMENTS] = bid.comment;
                bidObject[Constants.GUARANTEE] = bid.guarantee;
                bidObject[Constants.SHIPPING] = bid.shipping;
                bidObject[Constants.SIZE_STEP_1] = bid.size_step_1;
                bidObject[Constants.SIZE_STEP_2] = bid.size_step_2;
                bidObject[Constants.SIZE_STEP_3] = bid.size_step_3;
                bidObject[Constants.SIZE_STEP_4] = bid.size_step_4;
                bidObject[Constants.SIZE_STEP_5] = bid.size_step_5;

                Model model = new Model();
                ParseObject group = await model.retrieveGroup(groupId);

                bidObject[Constants.GROUP_BUYING] = group;


                bidObject[Constants.BUSINESS] = await model.retrieveBusiness(bid.businessId);

                await bidObject.SaveAsync();
                return true;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<IEnumerable<ParseObject>> retrieveBids(string groupId)
        {
            try
            {
                Model model = new Model();
                ParseObject group = await model.retrieveGroup(groupId);
                var bidsQuery = ParseObject.GetQuery(Constants.BID_TABLE).WhereEqualTo(Constants.GROUP_BUYING, group);
                
                IEnumerable<ParseObject> bids = await bidsQuery.FindAsync();

                return bids;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }       
        }

        public static async Task<ParseObject> getBid(ParseObject winningBid)
        {
            try
            {
                var bidQuery = ParseObject.GetQuery(Constants.BID_TABLE).WhereEqualTo(Constants.OBJECT_ID, winningBid.Get<ParseObject>(Constants.BID).ObjectId);
                ParseObject bid = await bidQuery.FirstAsync();

                return bid;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static double getBidPriceStep1(ParseObject bid)
        {
            return bid.Get<double>(Constants.PRICE_STEP_1);
        }
        public static double getBidPriceStep2(ParseObject bid)
        {
            return bid.Get<double>(Constants.PRICE_STEP_2);
        }
        public static double getBidPriceStep3(ParseObject bid)
        {
            return bid.Get<double>(Constants.PRICE_STEP_3);
        }
        public static double getBidPriceStep4(ParseObject bid)
        {
            return bid.Get<double>(Constants.PRICE_STEP_4);
        }
        public static double getBidPriceStep5(ParseObject bid)
        {
            return bid.Get<double>(Constants.PRICE_STEP_5);
        }
        public static double getBidGuarantee(ParseObject bid)
        {
            return bid.Get<double>(Constants.GUARANTEE);
        }
        public static double getBidOriginalPrice(ParseObject bid)
        {
            return bid.Get<double>(Constants.ORIGINAL_PRICE);
        }
        public static double getBidMaxUints(ParseObject bid)
        {
            return bid.Get<double>(Constants.MAX_UNITS);
        }
        public static bool getBidShipping(ParseObject bid)
        {
            return bid.Get<bool>(Constants.SHIPPING);

        }
        public static string getBusinessIdOfBid(ParseObject bid)
        {
            ParseObject businessOfBid = bid.Get<ParseObject>(Constants.BUSINESS);
            return businessOfBid.ObjectId;
        }
        public static string getCommentsOfBid(ParseObject bid)
        {
            return bid.Get<string>(Constants.COMMENTS);
        }


        public static async Task<ParseObject> retrieveBidByID(string bidId)
        {
            Model model = new Model();
            try
            {
                var bidQuery = ParseObject.GetQuery(Constants.BID_TABLE).WhereEqualTo(Constants.OBJECT_ID, bidId);
                ParseObject bid = await bidQuery.FirstAsync();
                return bid;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }


    }
}