﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DAL
{
    public class CategoryParse
    {
        public static async Task<IEnumerable<ParseObject>> retrieveCategories()
        {
            try
            {
                var CategoriesQuery = ParseObject.GetQuery(Constants.CATEGORY_TABLE);
                IEnumerable<ParseObject> categories = await CategoriesQuery.FindAsync();

                return categories;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }


        public static string getCategoryName(ParseObject category)
        {
            return category.Get<string>(Constants.NAME);
        }
    }
}