﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Parse;
using System.Threading.Tasks;

namespace DAL
{
    public class GroupParse
    {

        public static async Task<bool> deleteGroup(string groupId)
        {
            try
            {
                Model m = new Model();
                ParseObject group = await m.retrieveGroup(groupId);

                var query = ParseObject.GetQuery(Constants.PRODUCT_TABLE)
                    .WhereEqualTo(Constants.GROUP, group);

                ParseObject product = await query.FirstAsync();
                product.Remove(Constants.GROUP);

                await product.SaveAsync();
                await group.DeleteAsync();
                return true;
            } catch (ParseException e)
            {
                Console.WriteLine(e);
            }



            return false;
        }


        public static async Task<string> retrieveUserPref(string userId)
        {
            try
            {
                var query = ParseObject.GetQuery(Constants.PREF_TABLE)
                            .WhereEqualTo(Constants.USER_ID, userId);

                ParseObject pref = await query.FirstAsync();
                return pref.Get<string>(Constants.PREF);
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }


        public static async Task<IEnumerable<ParseObject>> retrieveAllActiveGroups()
        {
            try
            {
                var allGroupsQuery = ParseObject.GetQuery(Constants.GROUP_BUYING_TABLE).WhereEqualTo(Constants.ACTIVE, true);
                IEnumerable<ParseObject> allGroups = await allGroupsQuery.FindAsync();
                return allGroups;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<BLL.HomePageModel> retrieveGroupsForHomePage()
        {
            try
            {

                var groupQuery = ParseObject.GetQuery(Constants.GROUP_BUYING_TABLE)
                            .WhereEqualTo(Constants.ACTIVE, true)
                            .WhereNotEqualTo(Constants.WINNING_BID, null)
                            .Select(Constants.PRODUCT);

                var productQuery = ParseObject.GetQuery(Constants.PRODUCT_TABLE)
                    .WhereMatchesQuery(Constants.GROUP, groupQuery)
                    .Include(Constants.GROUP).Include(Constants.PRODUCT_CELLPHONE)
                    .Select(Constants.TITLE)
                    .Select(Constants.IMAGE)
                    .Select(Constants.GROUP)
                    .Select(Constants.PRODUCT_CELLPHONE);
              
                  
                IEnumerable<ParseObject> products = await productQuery.FindAsync();

                BLL.HomePageModel model = new BLL.HomePageModel();
                foreach (ParseObject p in products)
                {
                    string title = p.Get<string>(Constants.TITLE);
                    ParseFile file = p.Get<ParseFile>(Constants.IMAGE);
                    string imageUrl = (file == null) ? null : file.Url.ToString();
                    string productId = p.ObjectId;

                    ParseObject group = p.Get<ParseObject>(Constants.GROUP);
                    DateTime groupExpirationDate = group.Get<DateTime>(Constants.EXPIRATION_DATE);
                    string groupObjectId = group.ObjectId;

                    ParseObject productCellphone = p.Get<ParseObject>(Constants.PRODUCT_CELLPHONE);
                    double camera = productCellphone.Get<double>(Constants.BACK_CAMERA);
                    double storage = productCellphone.Get<double>(Constants.STORAGE_CAPACITY);

                    model.add(title, imageUrl, groupObjectId, groupExpirationDate, camera, storage, productId);
                }

                return model;

            }
            catch (Exception e) {
            }
            return null;
        }


        public static async Task<IEnumerable<ParseObject>> retrieveAllActiveGroupsWithoutWinningBid()
        {
            try
            {
                var allGroupsQuery = ParseObject.GetQuery(Constants.GROUP_BUYING_TABLE).WhereEqualTo(Constants.ACTIVE, true).WhereEqualTo(Constants.WINNING_BID, null);
                IEnumerable<ParseObject> allGroups = await allGroupsQuery.FindAsync();
                return allGroups;
            }
            catch (Exception e) { throw e; }
        }



        public static async Task<IEnumerable<ParseUser>> retrieveUsersOfGroup(ParseObject group)
        {
            try
            {
                var userRelation = group.GetRelation<ParseUser>(Constants.USERS);
                IEnumerable<ParseUser> groupUsers = await userRelation.Query.FindAsync();

                return groupUsers;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static async Task<ParseObject> retrieveProductOfGroup(ParseObject group)
        {
            try
            {
                //minProduct is just to obtain the product objectId
                ParseObject minProduct = group.Get<ParseObject>(Constants.PRODUCT);

                //product is a Pointer in the group, therefore we need to query him
                var productQuery = ParseObject.GetQuery(Constants.PRODUCT_TABLE).WhereEqualTo(Constants.OBJECT_ID, minProduct.ObjectId);
                ParseObject product = await productQuery.FirstAsync();

                return product;
            }
            catch (Exception e)
            {
                // add log
                throw e;
            }
        }

        public static async Task<string> createGroup(ParseObject product)
        {
            try
            {
                ParseObject newGroup = new ParseObject(Constants.GROUP_BUYING_TABLE);
                newGroup[Constants.PRODUCT] = product;
                newGroup[Constants.ACTIVE] = true; //we just created new group of this product so now we have an active group until the group will finished
                newGroup[Constants.EXPIRATION_DATE] = DateTime.Now.AddDays(Constants.GROUP_LIFE_TIME);
 
                await newGroup.SaveAsync();

                product[Constants.GROUP] = newGroup;
                await product.SaveAsync();

                return newGroup.ObjectId;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<ParseObject> retrieveGroup(string groupId)
        {
            try
            {
                var groupQuery = ParseObject.GetQuery(Constants.GROUP_BUYING_TABLE).WhereEqualTo(Constants.OBJECT_ID, groupId);
                ParseObject group = await groupQuery.FirstAsync();

                return group;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }
        
        public static DateTime getExpirationDate(ParseObject group)
        {
            return group.Get<DateTime>(Constants.EXPIRATION_DATE);
        }

        public static async Task<bool> setGroupActive(ParseObject group, bool status)
        {
            group[Constants.ACTIVE] = status;
            await group.SaveAsync();
            return true;
        }


        public static async Task<bool> checkAllGroupsExpireDate()
        {
            IEnumerable<ParseObject> allGroups = await retrieveAllActiveGroups();
            foreach(ParseObject p in allGroups)
            {
                if(p.Get<DateTime>(Constants.EXPIRATION_DATE) < DateTime.Now)
                {
                    p[Constants.ACTIVE] = false;
                    await p.SaveAsync();
                }
            }
            return true;
        }



        public static async Task<bool> joinToGroup(ParseObject group)
        {
            try
            {
                ParseUser currentUser = ParseUser.CurrentUser;
                var relation = group.GetRelation<ParseObject>(Constants.USERS);
                relation.Add(currentUser);
                await group.SaveAsync();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
            
        }


        public static async Task<bool> deleteProductFromBusiness(string productId)
        {
            try
            {
                Model m = new Model();
                ParseObject myBusiness = await m.retrieveMyBusiness();
                IEnumerable<ParseObject> myProducts = await m.retrieveAllMyPoducts(myBusiness.ObjectId);
                
                foreach (ParseObject p in myProducts)
                {
                    if (p.ObjectId == productId)
                    {
                        await p.DeleteAsync();
                        return true;
                    }

                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }

        }
    }
}