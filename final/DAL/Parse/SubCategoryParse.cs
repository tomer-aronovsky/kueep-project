﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DAL
{
    public class SubCategoryParse
    {
        public static async Task<IEnumerable<ParseObject>> retrieveSubCategories()
        {
            try
            {
                var SubCategoriesQuery = ParseObject.GetQuery(Constants.SUB_CATEGORY_TABLE);
                IEnumerable<ParseObject> subCategories = await SubCategoriesQuery.FindAsync();

                return subCategories;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static string getSubCategoryName(ParseObject subCategory)
        {
            return subCategory.Get<string>(Constants.NAME);
        }


        public static async Task<IEnumerable<ParseObject>> retrieveSubCategoriesOfCategory(string categoryId)
        {
            try
            {
                var category = ParseObject.GetQuery(Constants.CATEGORY_TABLE).WhereEqualTo(Constants.OBJECT_ID, categoryId);
                ParseObject category1 = await category.FirstAsync();

                var relation = category1.GetRelation<ParseObject>(Constants.SUB_CATEGORIES);
                IEnumerable<ParseObject> allSubCategories = await relation.Query.FindAsync();
                return allSubCategories;

            }
            catch (Exception e) { }

            return null;

        }

    }
}