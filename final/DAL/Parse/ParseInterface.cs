﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parse;

namespace DAL
{
    interface ParseInterface
    {
        //business
        Task<ParseObject> retrieveMyBusiness();
        Task<ParseObject> retrieveBusiness(string businessId);
        Task<bool> addProductToBusiness(string productId, ParseObject business);
        Task<bool> removeProductFromBusiness(string productId);
        ParseFile retrieveLogoOfBusiness(ParseObject business);
        Task<IEnumerable<ParseObject>> retrieveAllActiveGroupsForBusiness(string businessId);
        Task<bool> SaveBusiness(BLL.Business business);
        Task<bool> deleteProductFromBusiness(string ProductId);
        Task<IEnumerable<ParseObject>> retrieveGroupsBusinessWon(string businessId);
        Task<List<string>> getMailOfBusinessesWhoHaveThisProducts(string productId);



        //product
        Task<IEnumerable<ParseObject>> businessesWhoHaveThisProduct(ParseObject product);
        Task<IEnumerable<ParseObject>> retrieveAllProducts();
        Task<ParseObject> retrieveProduct(string productID); 
        Task<IEnumerable<ParseObject>> retrieveAllMyPoducts(string businessId);
        string getProductTitle(ParseObject product);
        Task<ParseObject> retrieveActiveGroupForProduct(string productId);
        ParseFile getProductImage(ParseObject product);
        Task<bool> addBusinessToProduct(string productId, ParseObject business);
        Task<bool> removeBusinessFromProduct(string productId, ParseObject business);
        Task<IEnumerable<ParseObject>> retrieveProductsOfSubCategory(string subCategoryId);
        Task<ParseObject> retrieveProductDetails(ParseObject parseProduct, string productTypeName);
        Task<ParseObject> retrieveManufacturer(ParseObject parseProduct);
        IEnumerable<ParseFile> getProductSliderImages(ParseObject parseProduct);

        //group
        Task<IEnumerable<ParseObject>> retrieveAllActiveGroups();
        Task<IEnumerable<ParseObject>> retrieveAllActiveGroupsWithoutWinningBid();
        Task<BLL.HomePageModel> retrieveGroupsForHomePage();
        Task<IEnumerable<ParseUser>> retrieveUsersOfGroup(ParseObject group);
        Task<ParseObject> retrieveProductOfGroup(ParseObject group);
        Task<ParseObject> retrieveGroup(string groupId);
        Task<string> createGroup(ParseObject product);
        DateTime getExpirationDate(ParseObject group);
        Task<bool> setGroupActive(ParseObject group, bool status);
        Task<bool> checkAllGroupsExpireDate();
        Task<bool> joinToGroup(ParseObject group);
        Task<string> retrieveUserPref(string userId);
        Task<bool> deleteGroup(string groupId);

        //winning bid
        Task<IEnumerable<ParseObject>> retrieveWinningBids();
        Task<ParseObject> retrieveBidOfWinningBid(ParseObject winningBid);
        Task<bool> createWinningBid(ParseObject bid, string groupId);
        Task<ParseObject> retrieveWinningBid(ParseObject group);
        Task<ParseObject> retrieveBusinessOfWinningBid(ParseObject winningBid);

        //bid
        Task<ParseObject> retrieveGroupOfBid(ParseObject bid);
        Task<bool> createBid(BLL.Bid bid, string groupId);
        Task<IEnumerable<ParseObject>> retrieveBids(string groupId);
        Task<ParseObject> getBid(ParseObject winningBid);
        double getBidPriceStep1(ParseObject bid);
        double getBidPriceStep2(ParseObject bid);
        double getBidPriceStep3(ParseObject bid);
        double getBidPriceStep4(ParseObject bid);
        double getBidPriceStep5(ParseObject bid);
        double getBidGuarantee(ParseObject bid);
        double getBidOriginalPrice(ParseObject bid);
        double getBidMaxUints(ParseObject bid);
        bool getBidShipping(ParseObject bid);
        string getBusinessIdOfBid(ParseObject bid);
        string getCommentsOfBid(ParseObject bid);
        Task<ParseObject> retrieveBidByID(string bidId);


        //category
        Task<IEnumerable<ParseObject>> retrieveCategories(); 
        string getCategoryName(ParseObject category);

        //subcategory
        Task<IEnumerable<ParseObject>> retrieveSubCategoriesOfCategory(string categoryId);
        Task<IEnumerable<ParseObject>> retrieveSubCategories();
        string getSubCategoryName(ParseObject subCategory);

        //user
        bool isBusiness(ParseUser user);
        Task<IEnumerable<ParseObject>> retrieveActiveGroupsUserBelongTo();
        Task<bool> saveUserCreditCardId(string creditCardId);
        string getCreditCatdId();
        Task<string> getCurrentUserSession();
        string getCurrentUserId();
        Task<bool> addUserToPref(string userID);
    }
}
