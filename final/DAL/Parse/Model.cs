﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using BLL;
using Parse;

namespace DAL
{
    public class Model : ParseInterface
    {
        ParseModel parseModel;

        //constructor
        public Model()
        {
            parseModel = new ParseModel();
        }

        public Task<bool> deleteGroup(string groupId)
        {
            return parseModel.deleteGroup(groupId);
        }

        //business
        public Task<List<string>> getMailOfBusinessesWhoHaveThisProducts(string productId)
        {
            return parseModel.getMailOfBusinessesWhoHaveThisProducts(productId);
        }
        public Task<bool> SaveBusiness(Business business)
        {
            return parseModel.SaveBusiness(business);
        }

        public Task<ParseObject> retrieveMyBusiness()
        {
            return parseModel.retrieveMyBusiness();
        }

        public Task<ParseObject> retrieveBusiness(string businessId)
        {
            return parseModel.retrieveBusiness(businessId);
        }

        public Task<bool> addProductToBusiness(string productId, ParseObject business)
        {
            return parseModel.addProductToBusiness(productId, business);
        }

        public Task<bool> removeProductFromBusiness(string productId)
        {
            return parseModel.removeProductFromBusiness(productId);
        }

        public Task<IEnumerable<ParseObject>> retrieveAllActiveGroupsForBusiness(string businessId)
        {
            return parseModel.retrieveAllActiveGroupsForBusiness(businessId);
        }
        public ParseFile retrieveLogoOfBusiness(ParseObject business)
        {
            return parseModel.retrieveLogoOfBusiness(business);
        }
        public Task<string> retrieveEmailFromBusiness(ParseObject business)
        {
            return parseModel.retrieveEmailFromBusiness(business);
        }
        public Task<IEnumerable<ParseObject>> retrieveGroupsBusinessWon(string businessId)
        {
            return parseModel.retrieveGroupsBusinessWon(businessId);
        }



        //product
        public Task<IEnumerable<ParseObject>> businessesWhoHaveThisProduct(ParseObject product)
        {
            return parseModel.businessesWhoHaveThisProduct(product);
        }

        public Task<IEnumerable<ParseObject>> retrieveAllProducts()
        {
            return parseModel.retrieveAllProducts();
        }
        public Task<ParseObject> retrieveProduct(string productID)
        {
            return parseModel.retrieveProduct(productID);

        }
        public Task<IEnumerable<ParseObject>> retrieveAllMyPoducts(string businessId)
        {
            return parseModel.retrieveAllMyPoducts(businessId);
        }
        public Task<ParseObject> retrieveActiveGroupForProduct(string productID)
        {
            return parseModel.retrieveActiveGroupForProduct(productID);
        }

        public string getProductTitle(ParseObject product)
        {
            return parseModel.getProductTitle(product);
        }
        public ParseFile getProductImage(ParseObject product)
        {
            return parseModel.getProductImage(product);
        }
        public Task<bool> addBusinessToProduct(string productId, ParseObject business)
        {
            return parseModel.addBusinessToProduct(productId, business);
        }
        public Task<bool> removeBusinessFromProduct(string productId, ParseObject business)
        {
            return parseModel.removeBusinessFromProduct(productId, business);
        }
        public Task<IEnumerable<ParseObject>> retrieveProductsOfSubCategory(string subCategoryId)
        {
            return parseModel.retrieveProductsOfSubCategory(subCategoryId);
        }

        public Task<ParseObject> retrieveProductDetails(ParseObject productParse, string productTypeName)
        {
            return parseModel.retrieveProductDetails(productParse, productTypeName);
        }
        public Task<ParseObject> retrieveManufacturer(ParseObject parseProduct)
        {
            return parseModel.retrieveManufacturer(parseProduct);
        }

        public IEnumerable<ParseFile> getProductSliderImages(ParseObject parseProduct)
        {
            return parseModel.getProductSliderImages(parseProduct);
        }
        //group
        public Task<string> retrieveUserPref(string userId)
        {
            return parseModel.retrieveUserPref(userId);
        }

        public Task<IEnumerable<ParseObject>> retrieveAllActiveGroups()
        {
            return parseModel.retrieveAllActiveGroups();
        }

        public Task<BLL.HomePageModel> retrieveGroupsForHomePage()
        {
            return parseModel.retrieveGroupsForHomePage();
        }

        public Task<IEnumerable<ParseObject>> retrieveAllActiveGroupsWithoutWinningBid()
        {
            return parseModel.retrieveAllActiveGroupsWithoutWinningBid();
        }

        public Task<IEnumerable<ParseUser>> retrieveUsersOfGroup(ParseObject group)
        {
            return parseModel.retrieveUsersOfGroup(group);
        }

        public Task<ParseObject> retrieveProductOfGroup(ParseObject group)
        {
            return parseModel.retrieveProductOfGroup(group);
        }

        public Task<string> createGroup(ParseObject product)
        {
            return parseModel.createGroup(product);

        }
        public Task<ParseObject> retrieveGroup(string groupId)
        {
            return parseModel.retrieveGroup(groupId);
        }

        public Task<bool> deleteProductFromBusiness(string productId)
        {
            return parseModel.deleteProductFromBusiness(productId);
        }

        
        public DateTime getExpirationDate(ParseObject group)
        {
            return parseModel.getExpirationDate(group);
        }
        public Task<bool> setGroupActive(ParseObject group, bool status)
        {
            return parseModel.setGroupActive(group, status);
        }

        public Task<bool> checkAllGroupsExpireDate()
        {
            return parseModel.checkAllGroupsExpireDate();
        }

        public Task<bool> joinToGroup(ParseObject group)
        {
            return parseModel.joinToGroup(group);
        }



        //winning bid
        public Task<IEnumerable<ParseObject>> retrieveWinningBids()
        {
            return parseModel.retrieveWinningBids();
        }

        public Task<ParseObject> retrieveBidOfWinningBid(ParseObject winningBid)
        {
            return parseModel.retrieveBidOfWinningBid(winningBid);
        }

        public Task<bool> createWinningBid(ParseObject bid, string groupId)
        {
            return parseModel.createWinningBid(bid, groupId);
        }

        public Task<ParseObject> retrieveWinningBid(ParseObject parseGroup)
        {
            return parseModel.retrieveWinningBid(parseGroup);
        }

        public Task<ParseObject>  retrieveBusinessOfWinningBid(ParseObject winningBid)
        {
            return parseModel.retrieveBusinessOfWinningBid(winningBid);
        }

        //bid
        public Task<ParseObject> retrieveGroupOfBid(ParseObject bid)
        {
            return parseModel.retrieveGroupOfBid(bid);
        }

        public Task<bool> createBid(BLL.Bid bid, string groupId)
        {
            return parseModel.createBid(bid, groupId);
        }

        public Task<IEnumerable<ParseObject>> retrieveBids(string groupId)
        {
            return parseModel.retrieveBids(groupId);
        }

        public Task<ParseObject> getBid(ParseObject bid)
        {
            return parseModel.getBid(bid);
        }
        public double getBidPriceStep1(ParseObject bid)
        {
            return parseModel.getBidPriceStep1(bid);
        }

        public double getBidPriceStep2(ParseObject bid)
        {
            return parseModel.getBidPriceStep2(bid);
        }

        public double getBidPriceStep3(ParseObject bid)
        {
            return parseModel.getBidPriceStep3(bid);
        }

        public double getBidPriceStep4(ParseObject bid)
        {
            return parseModel.getBidPriceStep4(bid);
        }

        public double getBidPriceStep5(ParseObject bid)
        {
            return parseModel.getBidPriceStep5(bid);
        }

        public double getBidGuarantee(ParseObject bid)
        {
            return parseModel.getBidGuarantee(bid);
        }

        public double getBidOriginalPrice(ParseObject bid)
        {
            return parseModel.getBidOriginalPrice(bid);
        }

        public double getBidMaxUints(ParseObject bid)
        {
            return parseModel.getBidMaxUints(bid);
        }

        public bool getBidShipping(ParseObject bid)
        {
            return parseModel.getBidShipping(bid);
        }

        public string getBusinessIdOfBid(ParseObject bid)
        {
            return parseModel.getBusinessIdOfBid(bid);
        }

        public string getCommentsOfBid(ParseObject bid)
        {
            return parseModel.getCommentsOfBid(bid);
        }

        public Task<ParseObject> retrieveBidByID(string bidId)
        {
            return parseModel.retrieveBidByID(bidId);
        }





        //categories
        public Task<IEnumerable<ParseObject>> retrieveCategories()
        {
            return parseModel.retrieveCategories();
        }
        public string getCategoryName(ParseObject category)
        {
            return parseModel.getCategoryName(category);
        }


        //sub categories
        public Task<IEnumerable<ParseObject>> retrieveSubCategories()
        {
            return parseModel.retrieveSubCategories();
        }
        public string getSubCategoryName(ParseObject subCategory)
        {
            return parseModel.getSubCategoryName(subCategory);
        }

        public Task<IEnumerable<ParseObject>> retrieveSubCategoriesOfCategory(string categoryId)
        {
            return parseModel.retrieveSubCategoriesOfCategory(categoryId);
        }

        //user
        public bool isBusiness(ParseUser user)
        {
            return parseModel.isBusiness(user);
        }

        public string getCurrentUserId()
        {
            return parseModel.getCurrentUserId();
        }

        public Task<IEnumerable<ParseObject>> retrieveActiveGroupsUserBelongTo()
        {
            return parseModel.retrieveActiveGroupsUserBelongTo();
        }

        public Task<string> getCurrentUserSession()
        {
            return parseModel.getCurrentUserSession();
        }

        public Task<bool> saveUserCreditCardId(string creditCardId)
        {
            return parseModel.saveUserCreditCardId(creditCardId);
        }

        public string getCreditCatdId()
        {
            return parseModel.getCreditCatdId();
        }

        public Task<bool> addUserToPref(string userID)
        {
            return parseModel.addUserToPref(userID);
        }



    }
}