﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Parse;
using System.Threading.Tasks;

namespace DAL
{
    public class ProductParse
    {


        public static async Task<IEnumerable<ParseObject>> businessesWhoHaveThisProduct(ParseObject product)
        {
            try
            {
                var relation = product.GetRelation<ParseObject>(Constants.BUSINESS);
                IEnumerable<ParseObject> businesses = await relation.Query.FindAsync();
                return businesses;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }



        public static async Task<IEnumerable<ParseObject>> retrieveAllProducts()
        {
            try
            {
                var ProductsQuery = ParseObject.GetQuery(Constants.PRODUCT_TABLE);
                IEnumerable<ParseObject> products = await ProductsQuery.FindAsync();

                return products;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<IEnumerable<ParseObject>> retrieveAllMyPoducts(string businessId)
        {
            try
            {
                Model model = new Model();
                ParseObject business = await model.retrieveBusiness(businessId);

                var relation = business.GetRelation<ParseObject>(Constants.PRODUCTS);
                IEnumerable<ParseObject> products = await relation.Query.FindAsync();

                return products;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<ParseObject> retrieveProduct(string productID)
        {
            try
            {
                var productQuery = ParseObject.GetQuery(Constants.PRODUCT_TABLE).WhereEqualTo(Constants.OBJECT_ID, productID);
                ParseObject product = await productQuery.FirstAsync();


                return product;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<ParseObject> retrieveActiveGroupForProduct(string productID)
        {
            try
            {
                ParseObject product = await retrieveProduct(productID);
                var productQuery = ParseObject.GetQuery(Constants.GROUP_BUYING_TABLE).WhereEqualTo(Constants.PRODUCT, product).WhereEqualTo(Constants.ACTIVE, true);
                ParseObject group = await productQuery.FirstAsync();


                return group;
            }
            catch (Exception e)
            {
                //add log
                return null;
            }
        }


        public static string getProductTitle(ParseObject product)
        {
            return product.Get<string>(Constants.TITLE);
        }

        public static ParseFile getProductImage(ParseObject product)
        {
            return product.Get<ParseFile>(Constants.IMAGE);
        }

        public static async Task<bool> addBusinessToProduct(string productId, ParseObject business)
        {
            try
            {
                //get product
                Model model = new Model();
                ParseObject product = await model.retrieveProduct(productId);

                var relation = product.GetRelation<ParseObject>(Constants.BUSINESS);
                relation.Add(business);
                await product.SaveAsync();

                return true;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<bool> removeBusinessFromProduct(string productId, ParseObject business)
        {
            try
            {
                //get product
                Model model = new Model();
                ParseObject product = await model.retrieveProduct(productId);

                var relation = product.GetRelation<ParseObject>(Constants.BUSINESS);
                relation.Remove(business);
                await product.SaveAsync();

                return true;
            }
            catch (Exception e)
            {
                //add log
                throw e;
            }
        }

        public static async Task<IEnumerable<ParseObject>> retrieveProductsOfSubCategory(string subCategoryId)
        {
            try
            {
                var subCategory = ParseObject.GetQuery(Constants.SUB_CATEGORY_TABLE).WhereEqualTo(Constants.OBJECT_ID, subCategoryId);
                ParseObject subCategory1 = await subCategory.FirstAsync();


                var query = ParseObject.GetQuery(Constants.PRODUCT_TABLE)
                    .WhereEqualTo("sub_category", subCategory1).WhereEqualTo(Constants.GROUP, null);
                //var relation = subCategory1.GetRelation<ParseObject>(Constants.PRODUCTS);
                //IEnumerable<ParseObject> allProducts = await relation.Query.FindAsync();

                IEnumerable<ParseObject> allProducts = await query.FindAsync();
                return allProducts;
            }
            catch (Exception e) { }

            return null;
        }


        public static async Task<ParseObject> retrieveProductDetails(ParseObject parseProduct, string productTypeName)
        {
            ParseObject productType = null;
            if (productTypeName.Equals(Constants.PRODUCT_CELLPHONE)) {
                try
                {
                    ParseObject productTypeInProduct = parseProduct.Get<ParseObject>(Constants.PRODUCT_CELLPHONE);

                    //product is a Pointer in the group, therefore we need to query him
                    var productTypeQuery = ParseObject.GetQuery(Constants.PRODUCT_CELLPHONE_TABLE).WhereEqualTo(Constants.OBJECT_ID, productTypeInProduct.ObjectId);
                    productType = await productTypeQuery.FirstAsync();

                    return productType;
                }
                catch (Exception e)
                {
                    // add log
                    throw e;
                }
            }
            else if (productTypeName.Equals(Constants.PRODUCT_TELEVISION)) {

            }
            return null;
        }

        public static async Task<ParseObject> retrieveManufacturer(ParseObject parseProduct)
        {
            try
            {
                ParseObject ManufacturerInProduct = parseProduct.Get<ParseObject>(Constants.MANUFACTURER_ID);

                //manufacturer is a Pointer in the product, therefore we need to query him
                var manufacturerQuery = ParseObject.GetQuery(Constants.MANUFACTURER_TABLE).WhereEqualTo(Constants.OBJECT_ID, ManufacturerInProduct.ObjectId);
                ParseObject man = await manufacturerQuery.FirstAsync();

                return man;
            }
            catch (Exception e)
            {
                // add log
                throw e;
            }
        }


        public static IEnumerable<ParseFile> getProductSliderImages(ParseObject parseProduct)
        {
            ParseFile slider1 = parseProduct.Get<ParseFile>(Constants.SLIDER_IMAGE_1);
            ParseFile slider2 = parseProduct.Get<ParseFile>(Constants.SLIDER_IMAGE_2);
            ParseFile slider3 = parseProduct.Get<ParseFile>(Constants.SLIDER_IMAGE_3);

            List<ParseFile> sliderImages = new List<ParseFile>();
            if (slider1 != null)
                sliderImages.Add(slider1);
            if (slider2 != null)
                sliderImages.Add(slider2);
            if (slider3 != null)
                sliderImages.Add(slider3);

            return sliderImages;
        }

    }
}