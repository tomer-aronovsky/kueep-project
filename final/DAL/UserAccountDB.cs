﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL
{
    public class UserAccountDB
    {
        public static async System.Threading.Tasks.Task<BLL.User> LoginUser(string username, string password)
        {
            Model model = new Model();
            BLL.User currentUser = new BLL.User();
            try
            {
                ParseUser user = await ParseUser.LogInAsync(username, password);
                // Login was successful....
                currentUser.loginSuccessfull = true;
                ParseObject business = null;
                if (model.isBusiness(user))
                    business = await model.retrieveMyBusiness();
                else business = null;
                IEnumerable<ParseObject> groups = await model.retrieveAllActiveGroups(); //With the machine learning - here we can change to get Relevants group only!
                currentUser = ConvertFromParseObject.ConvertParseToUserAccount(user, business, groups);

                var session = await ParseSession.GetCurrentSessionAsync();
                var token = session.SessionToken;
                //save the token in cookie
                HttpCookie cookie = new HttpCookie(Constants.COOKIE_NAME);
                cookie[Constants.COOKIE_VALUES_USERID] = username;
                cookie[Constants.COOKIE_VALUES_AUTH] = password;
                cookie[Constants.COOKIE_VALUES_SESSION] = token;
                cookie.Expires = DateTime.Now.AddYears(2);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception e)
            {
                return null;
            }
            return currentUser;
         }

        public static async System.Threading.Tasks.Task<string> getCurrentUserSession()
        {
            Model model = new Model();
            return await model.getCurrentUserSession();
        }


        public static async System.Threading.Tasks.Task<string> RegisterNewUser(string userName, string firstName, string lastName, string email, string password, bool isBusiness)
        {
            Model model = new Model();

            var user = new ParseUser()
            {
                Username = userName,
                Password = password,
                Email = email
            };
            user[Constants.FIRST_NAME] = firstName;
            user[Constants.LAST_NAME] = lastName;
            if (isBusiness)
            {
                user[Constants.IS_BUSINESS] = true;
                user[Constants.IS_CLIENT] = false;
            }
            else
            {
                user[Constants.IS_BUSINESS] = false;
                user[Constants.IS_CLIENT] = true;
            }


            try {
                await user.SignUpAsync();
                bool addToPref = await model.addUserToPref(user.ObjectId);
            }
            catch (Exception e){
                return e.Message;
            }

            
            return "sucessfull";
        }

        
        public static async System.Threading.Tasks.Task<BLL.User> RegisterNewUser()
        {
            Model model = new Model();
            Parse.ParseUser user = Parse.ParseUser.CurrentUser;
            BLL.User currentUser = ConvertFromParseObject.ConvertParseToUserAccount(user,null,null);
            return currentUser;
        }





        public static async System.Threading.Tasks.Task<BLL.User> ParseCurrentUserToBLL(string userID, string pass)
        {
            Model model = new Model();
            ParseObject business;
            Parse.ParseUser user = null;

            if (Parse.ParseUser.CurrentUser == null)
            {
                try
                {
                    user = await ParseUser.LogInAsync(userID, pass);
                    //user = await ParseUser.BecomeAsync(cookie[Constants.COOKIE_VALUES_SESSION]);
                    if (user != null){
                        business = await model.retrieveMyBusiness();
                        IEnumerable<ParseObject> groups = await model.retrieveActiveGroupsUserBelongTo();
                        BLL.User currentUser = ConvertFromParseObject.ConvertParseToUserAccount(user, business, groups);
                        return currentUser;
                    }
                }
                catch (Exception e){  }
            }
            else if(Parse.ParseUser.CurrentUser != null)
            {
                try
                {
                    user = ParseUser.CurrentUser;
                    if (user != null){
                        business = await model.retrieveMyBusiness();
                        IEnumerable<ParseObject> groups = await model.retrieveActiveGroupsUserBelongTo();
                        BLL.User currentUser = ConvertFromParseObject.ConvertParseToUserAccount(user, business, groups);
                        return currentUser;
                    }
                }
                catch (Exception e) { }
            }
            return null;
        }

        public static async System.Threading.Tasks.Task<ParseObject> ResetUserPassword(string email)
        {
            await ParseUser.RequestPasswordResetAsync(email);
            return null;
        }


    }
}