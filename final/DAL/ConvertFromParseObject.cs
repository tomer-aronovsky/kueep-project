﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;
using Parse;

namespace DAL
{
    //Class role: get parse objects and covert them to BLL entities. A "Convertor" class
    public class ConvertFromParseObject
    {
        public static BLL.Product ConvertParseToProduct(ParseObject parseProduct, ParseFile file, ParseObject manufacturer, IEnumerable<ParseFile> sliderImages)
        {
            BLL.Product product =  new BLL.Product();
            product.productID = parseProduct.ObjectId;
            product.title = parseProduct.Get<string>(Constants.TITLE);
            product.productTypeName = parseProduct.Get<string>(Constants.PRODUCT_TYPE_NAME);
            if (file != null)
                product.image = file.Url.ToString();
            else product.image = null;

            if(sliderImages != null)
            {
                var slider1 = sliderImages.ElementAt(0);
                var slider2 = sliderImages.ElementAt(1);
                var slider3 = sliderImages.ElementAt(2);
                if (slider1 != null)
                    product.sliderImage1 = slider1.Url.ToString();
                if (slider2 != null)
                    product.sliderImage2 = slider2.Url.ToString();
                if (slider3 != null)
                    product.sliderImage3 = slider3.Url.ToString();
            }


            if (manufacturer != null)
            {
                product.manufacturer.manufacturerID = manufacturer.ObjectId;
                product.manufacturer.name = manufacturer.Get<string>(Constants.MANUFACTURER_NAME);
            }

            return product;
        }



        public static BLL.GroupBuying convertParseToGroupBuyingHomePage(ParseObject parseGroup, ParseObject parseProduct, ParseFile file)
        {
            BLL.GroupBuying group = new BLL.GroupBuying();
            group.groupBuyingID = parseGroup.ObjectId;
            group.active = parseGroup.Get<bool>(Constants.ACTIVE);
            group.expirationDate = parseGroup.Get<DateTime>(Constants.EXPIRATION_DATE);
            group.product = ConvertParseToProduct(parseProduct, file, null, null);
            return group;
        }


        public static BLL.GroupBuying convertParseToGroupBuying(ParseObject parseGroup, ParseObject parseProduct, ParseFile file, ParseObject parseWinningBid, ParseObject parseBusiness, IEnumerable<ParseObject> parseBusinessProducts, IEnumerable<ParseObject> usersInGroup, ParseObject manufacturer, IEnumerable<ParseFile> sliderImages)
        {
            BLL.GroupBuying group = new BLL.GroupBuying();
            group.groupBuyingID = parseGroup.ObjectId;
            group.active = parseGroup.Get<bool>(Constants.ACTIVE);
            group.expirationDate = parseGroup.Get<DateTime>(Constants.EXPIRATION_DATE);
            group.product = ConvertParseToProduct(parseProduct, file, manufacturer, sliderImages);
            if (parseWinningBid != null){
                group.winningBid = convertParseToWinningBid(parseWinningBid, parseBusiness, parseBusinessProducts);
            }
            if(usersInGroup != null)
            {
                foreach (ParseUser u in usersInGroup)
                {
                    BLL.User user = ConvertParseToUserAccount(u, null, null);
                    group.usersInGroup.Add(user);
                }
            }
            
            return group;
        }

        public static BLL.GroupBuying convertParseToProductType(BLL.GroupBuying group, ParseObject productType)
        {
            if (group.product.productTypeName.Equals(Constants.PRODUCT_CELLPHONE))
            {
                BLL.SpecificProductDetails.ProductCellphone cellphone = new BLL.SpecificProductDetails.ProductCellphone();
                cellphone.backCamera = productType.Get<double>(Constants.BACK_CAMERA);
                cellphone.frontCamera = productType.Get<double>(Constants.FRONT_CAMERA);
                cellphone.operationSystem = productType.Get<string>(Constants.OPERATION_SYSTEM);
                cellphone.screenSize = productType.Get<string>(Constants.SCREEN_SIZE);
                cellphone.storageCapacity = productType.Get<string>(Constants.STORAGE_CAPACITY);
                group.product.productCellphone = cellphone;
                return group;
            }
            else if (group.product.productTypeName.Equals(Constants.PRODUCT_TELEVISION)) // more product types will be here...
            {
                
            }

            return null;
        }




        public static BLL.Product convertParseProductToProductType(BLL.Product product, ParseObject productType)
        {
            if (product.productTypeName.Equals(Constants.PRODUCT_CELLPHONE))
            {
                BLL.SpecificProductDetails.ProductCellphone cellphone = new BLL.SpecificProductDetails.ProductCellphone();
                cellphone.backCamera = productType.Get<double>(Constants.BACK_CAMERA);
                cellphone.frontCamera = productType.Get<double>(Constants.FRONT_CAMERA);
                cellphone.operationSystem = productType.Get<string>(Constants.OPERATION_SYSTEM);
                cellphone.screenSize = productType.Get<string>(Constants.SCREEN_SIZE);
                cellphone.storageCapacity = productType.Get<string>(Constants.STORAGE_CAPACITY);
                product.productCellphone = cellphone;
                return product;
            }
            else if (product.productTypeName.Equals(Constants.PRODUCT_TELEVISION)) // more product types will be here...
            {

            }

            return null;
        }


        private static BLL.Bid convertParseToWinningBid(ParseObject parseWinningBid, ParseObject parseBusiness, IEnumerable<ParseObject> parseBusinessProducts)
        {
            BLL.Bid winningBid = convertParseToBid(parseWinningBid, parseBusiness);
            return winningBid;

        }

        
        public static BLL.ProductCategory convertParseToProductCategory(ParseObject parseProductCategory)
        {
            BLL.ProductCategory category = new BLL.ProductCategory();
            category.productCategoryID = parseProductCategory.ObjectId;
            category.name = parseProductCategory.Get<string>(Constants.NAME);
            return category;
        }

        public static BLL.ProductSubCategory convertParseToProductSubCategory(ParseObject parseProductSubCategory)
        {
            BLL.ProductSubCategory subCategory = new BLL.ProductSubCategory();
            subCategory.productSubCategoryID = parseProductSubCategory.ObjectId;
            subCategory.name = parseProductSubCategory.Get<string>(Constants.NAME);
            return subCategory;
        }


        public static BLL.Business convertParseToBusiness(ParseObject parseBusiness, IEnumerable<ParseObject> parseProductsBusiness, ParseFile logo)
        {
            if (parseBusiness == null)
            {
                return null;
            }
            BLL.Business business = new BLL.Business();
            business.businessID = parseBusiness.ObjectId;
            business.address = parseBusiness.Get<string>(Constants.ADRESS);
            business.facebookPage = parseBusiness.Get<string>(Constants.FACEBOOK_PAGE);
            business.phoneNumber = parseBusiness.Get<string>(Constants.PHONE_NUMBER);
            business.businessName = parseBusiness.Get<string>(Constants.NAME);
            business.website = parseBusiness.Get<string>(Constants.WEBSITE);
            business.aboutUs = parseBusiness.Get<string>(Constants.ABOUT_US);
            if (logo != null)
                business.businessLogo = logo.Url.ToString();
            else business.businessLogo = null;

            business.products = new List<BLL.Product>();
            foreach(ParseObject p in parseProductsBusiness)
            {
                ParseFile file = p.Get<ParseFile>(Constants.IMAGE);
                BLL.Product product = ConvertParseToProduct(p, file, null, null);
                business.products.Add(product);
            }
            return business;
        }
        

        public static async System.Threading.Tasks.Task<BLL.GroupBuying> convertParseWaitingForBids(ParseObject group, IEnumerable<ParseObject> bids)
        {
            BLL.GroupBuying groupWaiting = new BLL.GroupBuying();
            groupWaiting.groupBuyingID = group.ObjectId;
            groupWaiting.active = group.Get<bool>(Constants.ACTIVE);

            foreach (ParseObject bid in bids)
            {
                Model m = new Model();
                ParseObject bus = await m.retrieveBusiness(m.getBusinessIdOfBid(bid)); 
                BLL.Bid bid1 = convertParseToBid(bid, bus);
                groupWaiting.bids.Add(bid1);
            }

            return groupWaiting;
        }



        public static BLL.Bid convertParseToBid(ParseObject parseBid, ParseObject business)
        {
            BLL.Bid bid = new BLL.Bid();
            bid.bidID = parseBid.ObjectId;
            bid.comment = parseBid.Get<string>(Constants.COMMENTS);
            bid.max_units = parseBid.Get<int>(Constants.MAX_UNITS);
            bid.price_step_1 = parseBid.Get<double>(Constants.PRICE_STEP_1);
            bid.price_step_2 = parseBid.Get<double>(Constants.PRICE_STEP_2);
            bid.price_step_3 = parseBid.Get<double>(Constants.PRICE_STEP_3);
            bid.price_step_4 = parseBid.Get<double>(Constants.PRICE_STEP_4);
            bid.price_step_5 = parseBid.Get<double>(Constants.PRICE_STEP_5);
            bid.guarantee = parseBid.Get<double>(Constants.GUARANTEE);
            bid.original_price = parseBid.Get<double>(Constants.ORIGINAL_PRICE);
            bid.shipping = parseBid.Get<bool>(Constants.SHIPPING);
            bid.size_step_1 = parseBid.Get<double>(Constants.SIZE_STEP_1);
            bid.size_step_2 = parseBid.Get<double>(Constants.SIZE_STEP_2);
            bid.size_step_3 = parseBid.Get<double>(Constants.SIZE_STEP_3);
            bid.size_step_4 = parseBid.Get<double>(Constants.SIZE_STEP_4);
            bid.size_step_5 = parseBid.Get<double>(Constants.SIZE_STEP_5);
            if(business != null)
                bid.businessId = business.ObjectId;
            return bid;
        }


        public static BLL.User ConvertParseToUserAccount(ParseUser userParse, ParseObject business, IEnumerable<ParseObject> groups)
        {
            BLL.User user = new BLL.User();
            user.userID = userParse.ObjectId;
            user.firstName = userParse.Get<string>(Constants.FIRST_NAME);
            user.lastName = userParse.Get<string>(Constants.LAST_NAME);
            user.isBusiness = userParse.Get<bool>(Constants.IS_BUSINESS);
            user.isClient = userParse.Get<bool>(Constants.IS_CLIENT);
            user.username = userParse.Get<string>(Constants.USER_NAME);
            user.email = userParse.Get<string>(Constants.EMAIL);
            if (business != null)
                user.businessID = business.ObjectId;
            else user.businessID = null;
            if (groups != null){
                foreach(ParseObject g in groups){
                    user.groupsIDs.Add(g.ObjectId);
                }   
            }
            else user.groupsIDs = null;
            return user;
        }
    }
}


    
