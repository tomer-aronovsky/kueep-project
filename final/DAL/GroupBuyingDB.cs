﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;
using Parse;

namespace DAL
{
    public class GroupBuyingDB
    {

        //load home page active groups 
        public static async System.Threading.Tasks.Task<BLL.HomePageModel> loadHomePageGroups(BLL.User currentUser)
        {
            Model m = new Model();
            BLL.HomePageModel homePageModel = new BLL.HomePageModel();


            homePageModel = await m.retrieveGroupsForHomePage();
            homePageModel.products.Sort((x, y) => y.title.CompareTo(x.title));
            //get user pref
            if (currentUser != null)
            {
                string userPref = await m.retrieveUserPref(currentUser.userID);
                if (userPref != "null")
                {
                    homePageModel.sortAccordingToPref(userPref);
                }
            }
 
            return homePageModel;
        }

        public static async System.Threading.Tasks.Task<List<string>> getMailOfBusinessWithThisProduct(string productId)
        {
            Model model = new Model();
            return await model.getMailOfBusinessesWhoHaveThisProducts(productId);
        }


        //check if spesipic product already have active group right now
        public static async System.Threading.Tasks.Task<bool> AlreadyHaveActiveGroupForThisProduct(string selectedProductID)
        {
            Model model = new Model();
            IEnumerable<ParseObject> groups = await model.retrieveAllActiveGroups();
            foreach (ParseObject group in groups)
            {
                ParseObject product = await model.retrieveProductOfGroup(group);
                if (product.ObjectId == selectedProductID)
                    return true;
            }
            return false;
        }


        //create new group
        public static async System.Threading.Tasks.Task<GroupAndModel> CreateNewGroup(string selectedProductID)
        {
            Model model = new Model();
            ParseObject product = await model.retrieveProduct(selectedProductID);
            string groupId = await model.createGroup(product);
            BLL.GroupAndModel groupAndModel = new GroupAndModel();
            groupAndModel.groupId = groupId;
            groupAndModel.productName = product.Get<string>(Constants.TITLE);
            return groupAndModel;
        }


        // load group
        public static async System.Threading.Tasks.Task<BLL.GroupBuying> loadGroup(string groupId, string productTypeName)
        {
            Model model = new Model();
            ParseObject parseGroup = await model.retrieveGroup(groupId);
            ParseObject parseProduct = await model.retrieveProductOfGroup(parseGroup);
            ParseFile file = model.getProductImage(parseProduct);
            ParseObject productType = await model.retrieveProductDetails(parseProduct, productTypeName);
            ParseObject winningBid = await model.retrieveWinningBid(parseGroup);
            ParseObject business = await model.retrieveBusinessOfWinningBid(winningBid);
            ParseObject manufacturer = await model.retrieveManufacturer(parseProduct);
            IEnumerable<ParseUser> usersInGroup = await model.retrieveUsersOfGroup(parseGroup);
            IEnumerable<ParseFile> productSliderImages = model.getProductSliderImages(parseProduct);

            BLL.GroupBuying group = ConvertFromParseObject.convertParseToGroupBuying(parseGroup, parseProduct, file, winningBid, business, null, usersInGroup, manufacturer, productSliderImages);
            group.product.productTypeName = productTypeName;
            group = ConvertFromParseObject.convertParseToProductType(group, productType);


            return group;
        }

        // load group by product
        public static async System.Threading.Tasks.Task<BLL.GroupBuying> loadGroupByProduct(string productID)
        {
            Model model = new Model();
            ParseObject parseGroup = await model.retrieveActiveGroupForProduct(productID);
            if (parseGroup == null)
                return null;
            ParseObject parseProduct = await model.retrieveProduct(productID);
            ParseFile file = model.getProductImage(parseProduct);
            BLL.GroupBuying group = ConvertFromParseObject.convertParseToGroupBuying(parseGroup, parseProduct, file, null, null, null, null, null, null);

            return group;
        }


        public static async System.Threading.Tasks.Task<bool> JoinToGroup(string groupId)
        {
            Model model = new Model();
            ParseObject parseGroup = await model.retrieveGroup(groupId);
            if (await model.joinToGroup(parseGroup))
            {

            }

            return true;
        }












        //run all groups, check expire date and if needed change active to false
        public static void checkAllGroupsExpireDate()
        {
            Model model = new Model();
            model.checkAllGroupsExpireDate();
        }






    }
}