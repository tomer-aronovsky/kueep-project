﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Parse;
using System.IO;
using WebsiteMVC.Models;

namespace WebsiteMVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Parse initialization here
            ParseClient.Initialize(Constants.APPLICATION_ID, Constants.PARSE_CODE);
            ParseUser.EnableRevocableSessionAsync();
            ParseFacebookUtils.Initialize("1513033632350091");
            ParseFacebookUtils.Initialize("578999652278537");

            //log4net init
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));

            //check groups expiration date and change "active" to false in needed
            DAL.GroupBuyingDB.checkAllGroupsExpireDate();

        }
    }
}
