﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Parse;
using System.Threading.Tasks;
using WebsiteMVC.App_Start;

namespace WebsiteMVC.Controllers
{
    public class BusinessController : Controller
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        DAL.Model model = new DAL.Model();

        //Dashboard Index page
        [LoggingFilterAttribute]
        public async System.Threading.Tasks.Task<ActionResult> Dashboard()
        {
            Models.BusinessDashboardModel m = new Models.BusinessDashboardModel();
            m.business = await DAL.BusinessDashboardDB.loadBusiness();
            return View(m);
        }

        //return list of groups - waiting for bid! - that are relevant for this current business
        public async System.Threading.Tasks.Task<ActionResult> GroupsWitingForBid(string businessID)
        {
            Models.BusinessDashboardModel m = new Models.BusinessDashboardModel();
            m.groupsWaitingForBids = await DAL.BusinessDashboardDB.loadGroupsWaitingForBids(businessID);
            return View(m);
        }


        //return a list of groups that the current business "won".
        public async System.Threading.Tasks.Task<ActionResult> MyActiveGroups(string businessID)
        {
            Models.BusinessDashboardModel m = new Models.BusinessDashboardModel();
            m.myActiveGroups = await DAL.BusinessDashboardDB.loadGroupsThatBusinessWon(businessID);
            return View(m);
        }

        //list of product belongs to business
        public async System.Threading.Tasks.Task<ActionResult> MyProducts(string businessID)
        {
            Models.BusinessDashboardModel m = new Models.BusinessDashboardModel();
            m.myProducts = await DAL.BusinessDashboardDB.loadAllProducts(businessID);
            return View(m);
        }


        //add product to business
        public async System.Threading.Tasks.Task<ActionResult> AddProductToBusiness()
        {
            Models.BusinessDashboardModel m = new Models.BusinessDashboardModel();
            m.addProductToBusiness.products = await DAL.ProductDB.getAllProducts(true);
            m.addProductToBusiness.productCategories = await DAL.ProductDB.getAllProductCategories();
            m.addProductToBusiness.productSubCategories = await DAL.ProductDB.getAllProductSubCategories();
            return View(m);
        }

        //update business settings
        public async System.Threading.Tasks.Task<ActionResult> Settings()
        {
            Models.BusinessDashboardModel m = new Models.BusinessDashboardModel();
            m.business = await DAL.BusinessDashboardDB.loadBusiness();
            return View(m);
        }






        public ActionResult CompleteBusinessDetails()
        {
            Models.BusinessDashboardModel m = new Models.BusinessDashboardModel();
            return View(m);
        }

        public async System.Threading.Tasks.Task<ActionResult> SaveBusinessDetails(FormCollection colls, HttpPostedFileBase logo)
        {
            BLL.Business business = new BLL.Business();
            business.businessID = "0";
            business.businessName = colls["businessName"];
            business.website = colls["website"];
            business.phoneNumber = colls["phone"];
            business.facebookPage = colls["facebookPage"];
            business.aboutUs = colls["aboutUs"];
            business.address = colls["address"];
            if (logo != null)
                business.logo = logo;
            bool x = await DAL.BusinessDashboardDB.SaveBusiness(business);
            if (x)
            {
                Navigation.State.CurrentUser.businessID = business.businessID;
            }
            return RedirectToAction("Dashboard");
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> UpdateBusinessSettings(FormCollection colls, HttpPostedFileBase logo)
        {
            BLL.Business business = new BLL.Business();
            business.businessID = colls["businessID"];
            business.businessName = colls["businessName"];
            business.website = colls["website"];
            business.phoneNumber = colls["phone"];
            business.facebookPage = colls["facebookPage"];
            business.aboutUs = colls["aboutUs"];
            business.address = colls["address"];
            if (logo != null)
            {
                business.logo = logo;
            }
            bool x = await DAL.BusinessDashboardDB.UpdateBusinessSettings(business);
            if (x)
            {
                Navigation.State.CurrentUser.businessID = business.businessID;
            }
            return RedirectToAction("Dashboard");
        }


        public async System.Threading.Tasks.Task<bool> RemoveProductFromBusiness(string productId)
        {
            return await model.removeProductFromBusiness(productId);     
        }


        
        

        



        [HttpPost]
        public async Task<JsonResult> LoadProductDetails(string productId)
        {
            BLL.Product product = await DAL.ProductDB.LoadProduct(productId);     
            return Json(product);
        }

        public async System.Threading.Tasks.Task<ActionResult> SaveProductToBusiness(FormCollection coll)
        {
            //User wants to open new group of those parameters:
            string selectedCategory = coll[Constants.CATEGORY_ID];
            string selectedSubCategory = coll[Constants.SUB_CATEGORY_ID];
            string selectedProduct = coll[Constants.PRODUCT_ID];
            ParseObject myBusiness = await model.retrieveMyBusiness();
                
            await model.addProductToBusiness(selectedProduct, myBusiness);
            await model.addBusinessToProduct(selectedProduct, myBusiness);
            return RedirectToAction("Dashboard", "Business");
        }


    }
}

