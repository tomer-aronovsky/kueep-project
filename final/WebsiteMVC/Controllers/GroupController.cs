﻿using WebsiteMVC.Models;
using Parse;
using ScheduledTaskExample.ScheduledTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using WebsiteMVC.App_Start;

namespace WebsiteMVC.Controllers
{
    public class GroupController : Controller
    {
        DAL.Model model = new DAL.Model();

        //create group page - return the values of the select lists in create group view
        [LoggingFilterAttribute]
        public async System.Threading.Tasks.Task<ActionResult> CreateGroup()
        {
            Models.CreateGroupModel m = new Models.CreateGroupModel();
            m.products = await DAL.ProductDB.getAllProducts(false);
            m.productCategories = await DAL.ProductDB.getAllProductCategories();
            m.productSubCategories = await DAL.ProductDB.getAllProductSubCategories();
            return View(m);
        }



        public void sendMail(List<string> businessMailAddresses, BLL.GroupAndModel groupAndModel)
        {
            string mailContent;
            mailContent = "<html><body dir='rtl' style='font-family:arial;'>";
            mailContent += "נפתחה קבוצה חדשה עבור מוצר: " + groupAndModel.productName + "<br/>";
            mailContent += "<br/><b> הנך מוזמן להגיש הצעת מחיר </b> ";
            string url = "http://www.kueep.com/Bid/CreateBidLink?groupId=" + groupAndModel.groupId + "&productName=" + groupAndModel.productName;
            string encodedUrl = url.Replace(" ", "%20");
            mailContent += "<a href="+ encodedUrl + ">כאן</a>";
            mailContent += "<br/><br/><br/><br/>";
            mailContent += "זכור לתת הצעת מחיר תחרותית וכך תעלה את הסיכויים לזכות.";
            mailContent += "<br/>בהצלחה!";
            mailContent += "</body></html>";

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            //mail.To.Add(adresses);
            foreach (string address in businessMailAddresses)
                mail.Bcc.Add(address);
            mail.From = new System.Net.Mail.MailAddress("no-replay@kueep.com", "Kueep");
            mail.IsBodyHtml = true;
            mail.Subject = "kueep - קבוצה חדשה נפתחה";
            mail.Body = mailContent;

            System.Net.Mail.SmtpClient x = new System.Net.Mail.SmtpClient();
            x.Host = "smtp.gmail.com";
            x.Port = 587;
            x.Credentials = new System.Net.NetworkCredential("kueep.bids@gmail.com", "kueepbids123");
            x.EnableSsl = true;
            try
            {
                x.Send(mail);
            } catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        //[Authorize]
        public async Task<ActionResult> OpenGroup(FormCollection coll)
        {
            //User wants to open new group of those parameters
            string selectedCategory = coll[Constants.CATEGORY_ID];
            string selectedSubCategory = coll[Constants.SUB_CATEGORY_ID];
            string selectedProductId = coll[Constants.PRODUCT_ID];

            //check if we have active group for the same product
            bool AlreadyHaveActiveGroupForThisProduct = await DAL.GroupBuyingDB.AlreadyHaveActiveGroupForThisProduct(selectedProductId);

            //if we don't have active group right now of this product, lets create a new one
            if (!AlreadyHaveActiveGroupForThisProduct) {
                BLL.GroupAndModel groupAndModel = await DAL.GroupBuyingDB.CreateNewGroup(selectedProductId);

                //send mail to all business who have this product
                //with a link to make an offer
                List<string> businessMailAddresses = await DAL.GroupBuyingDB.getMailOfBusinessWithThisProduct(selectedProductId);
                if (businessMailAddresses!=null)
                    sendMail(businessMailAddresses, groupAndModel);

                //winning bid scheduler and deactivating group scheduler 
                startSchedulers(selectedProductId);

                return await GroupPage(selectedProductId, true);
            }
            return await GroupPage(selectedProductId, false);
        }

        //[Authorize]
        public async Task<ActionResult> GroupPage(string selectedProduct, bool createdNow)
        {
            Models.GroupBuyingModel m = new Models.GroupBuyingModel();
            m.group = await DAL.GroupBuyingDB.loadGroupByProduct(selectedProduct);
            m.groupCreatedNow = createdNow;
            if (createdNow)
            {
                m.timeToOffer = DateTime.UtcNow.AddMinutes(Constants.MAX_TIME_GRPUO_OFFER);
                return View("NewGroup", m);
            }

            return View("Group", m);
        }


        private async void startSchedulers(string selectedProduct)
        {
            //winning bid scheduler
            ParseObject group = await model.retrieveActiveGroupForProduct(selectedProduct);
            DateTime openingTime = (DateTime)group.CreatedAt;
            openingTime.ToLocalTime();
            WinningBidJob.Start(group.ObjectId, openingTime);

            //deactivate group scheduler
            DateTime expirationDate = model.getExpirationDate(group);
            DeactivateGroupJob.Start(group.ObjectId, expirationDate);
        }

        public async void deactivateGroup(string groupId)
        {
            ParseObject group = await model.retrieveGroup(groupId);
            await model.setGroupActive(group, false);
        }


        //The group Page
        [LoggingFilterAttribute]
        public async Task<ActionResult> Group(string groupId, string productTypeName)
        {
            Models.GroupBuyingModel m = new Models.GroupBuyingModel();
            m.group = await DAL.GroupBuyingDB.loadGroup(groupId, productTypeName);
            m.business = await DAL.BusinessDashboardDB.loadBusinessByID(m.group.winningBid.businessId);
            if (m.group.usersInGroup.Count() < m.group.winningBid.size_step_1)
                m.totalPrice = m.group.winningBid.price_step_1;
            else if (m.group.usersInGroup.Count < m.group.winningBid.size_step_1 + m.group.winningBid.size_step_2)
                m.totalPrice = m.group.winningBid.price_step_2;
            else if (m.group.usersInGroup.Count < m.group.winningBid.size_step_3 + m.group.winningBid.size_step_2 + m.group.winningBid.size_step_1)
                m.totalPrice = m.group.winningBid.price_step_3;
            else if (m.group.usersInGroup.Count < m.group.winningBid.size_step_4 + m.group.winningBid.size_step_3 + m.group.winningBid.size_step_2 + m.group.winningBid.size_step_1)
                m.totalPrice = m.group.winningBid.price_step_4;
            else m.totalPrice = m.group.winningBid.price_step_5;

            return View(m);
        }



        public async Task<ActionResult> JoinGroup(string groupId, string productTypeName)
        {
            bool join = await DAL.GroupBuyingDB.JoinToGroup(groupId);
            Models.GroupBuyingModel m = new Models.GroupBuyingModel();
            m.group = await DAL.GroupBuyingDB.loadGroup(groupId, productTypeName);
            m.business = await DAL.BusinessDashboardDB.loadBusinessByID(m.group.winningBid.businessId);
            if (m.group.usersInGroup.Count() < m.group.winningBid.size_step_1)
                m.totalPrice = m.group.winningBid.price_step_1;
            else if (m.group.usersInGroup.Count < m.group.winningBid.size_step_1 + m.group.winningBid.size_step_2)
                m.totalPrice = m.group.winningBid.price_step_2;
            else if (m.group.usersInGroup.Count < m.group.winningBid.size_step_3 + m.group.winningBid.size_step_2 + m.group.winningBid.size_step_1)
                m.totalPrice = m.group.winningBid.price_step_3;
            else if (m.group.usersInGroup.Count < m.group.winningBid.size_step_4 + m.group.winningBid.size_step_3 + m.group.winningBid.size_step_2 + m.group.winningBid.size_step_1)
                m.totalPrice = m.group.winningBid.price_step_4;
            else m.totalPrice = m.group.winningBid.price_step_5;


            return View("Group", m);
        }












        // ------- Ajax calls to fill select options ------- //
        [HttpPost]
        public async Task<JsonResult> LoadSubCategories(string categoryId)
        {
            List<BLL.ProductSubCategory> relevantSubCategories = await DAL.ProductDB.LoadSubCategories(categoryId);
            return Json(relevantSubCategories);
        }

        [HttpPost]
        public async Task<JsonResult> LoadProducts(string subCategoryId)
        {
            List<BLL.Product> relevantProducts = await DAL.ProductDB.LoadProducts(subCategoryId);
            relevantProducts.OrderBy(x => x.title);
            return Json(relevantProducts);
        }
    }
}






