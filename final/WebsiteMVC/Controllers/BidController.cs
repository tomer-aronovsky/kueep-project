﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Razor.Parser.SyntaxTree;
using WebsiteMVC.App_Start;

namespace WebsiteMVC.Controllers
{

    public class BidController : Controller
    {
        private static readonly SemaphoreSlim _sl = new SemaphoreSlim(initialCount: 1);

        // GET: Bid
        public ActionResult Index()
        {
            return View();
        }

        [LoggingFilterAttribute]
        public ActionResult CreateBidLink(string groupId, string productName)
        {
            Models.BidModel m = new Models.BidModel();
            m.groupBuyingId = groupId;
            m.productName = productName;
            return View(m);
        }

        public async System.Threading.Tasks.Task<ActionResult> BidDetails(string bidId)
        {
            Models.BidModel m = new Models.BidModel();
            m.bid = await DAL.BidDB.BidDetails(bidId);
            return View(m);
        }

        /*
        step sizes are calculeted as following:
        - first step: 10% of max units
        - second step: also 10% of max units
        
        - now we take the units left. if bigger than 15, we devide by 3 and split equally between steps 3,4,5.
        if smaller than 15, we devide by 2 and split equally between steps 3,4.

        step 5 is not mandatory.
            
        */
        private double[] calculateStepSizes(int maxUints)
        {
            int unitsLeft = maxUints;
            int[] result = new int[5];
            //ceiling round's up the number
            result[0] = (int)Math.Ceiling(maxUints * 0.1);
            result[1] = result[0];

            unitsLeft = unitsLeft - (result[0] + result[1]);
            if (unitsLeft > 15)
            {
                int tmp = (int)Math.Ceiling((double)unitsLeft / 3);
                result[2] = tmp;
                result[3] = tmp;
                result[4] = unitsLeft - result[2] - result[3];
            }
            else
            {
                int tmp = (int)Math.Ceiling((double)unitsLeft / 2);
                result[2] = tmp;
                result[3] = unitsLeft - result[2];
            }
            double[] doubleResult = new double[5];
            for (int i = 0; i < result.Length; i++)
                doubleResult[i] = (double)result[i];

            return doubleResult;
        }


        public async System.Threading.Tasks.Task<ActionResult> CreateBid(FormCollection coll)
        {

            //Asynchronously wait to enter the Semaphore. If no-one has been granted access to the Semaphore, code execution will proceed, otherwise this thread waits here until the semaphore is released 
            await _sl.WaitAsync();
            try
            {
                double maxUints = Convert.ToDouble(coll[Constants.MAX_UNITS]);
                double originalPrice = Convert.ToDouble(coll[Constants.ORIGINAL_PRICE]);
                double priceStep1 = Convert.ToDouble(coll[Constants.PRICE_STEP_1]);
                double priceStep2 = Convert.ToDouble(coll[Constants.PRICE_STEP_2]);
                double priceStep3 = Convert.ToDouble(coll[Constants.PRICE_STEP_3]);
                double priceStep4 = Convert.ToDouble(coll[Constants.PRICE_STEP_4]);
                double priceStep5 = Convert.ToDouble(coll[Constants.PRICE_STEP_5]);
                string comments = coll[Constants.COMMENTS];
                double guarantee = Convert.ToDouble(coll[Constants.GUARANTEE]);
                bool shipping = (coll[Constants.SHIPPING]) == "on";

                string groupId = coll["groupId"];

                double[] stepSizes = calculateStepSizes((int)maxUints);

                BLL.Bid bid = new BLL.Bid();

                bid.max_units = maxUints;
                bid.original_price = originalPrice;
                bid.price_step_1 = priceStep1;
                bid.price_step_2 = priceStep2;
                bid.price_step_3 = priceStep3;
                bid.price_step_4 = priceStep4;
                bid.price_step_5 = priceStep5;
                bid.size_step_1 = stepSizes[0];
                bid.size_step_2 = stepSizes[1];
                bid.size_step_3 = stepSizes[2];
                bid.size_step_4 = stepSizes[3];
                bid.size_step_5 = stepSizes[4];
                bid.guarantee = guarantee;
                bid.shipping = shipping;
                bid.comment = comments;

                await DAL.BidDB.createBid(bid, groupId);
            }
            finally
            {
                _sl.Release();
            }



            return RedirectToAction("Dashboard", "Business");
        }

        /*
        winning bid is determined by several parameters:
        1. select biggest difference between second and third prices steps, or between 3h and 4h.
        2. lowest price of the product (depends on max units)
        3. original price
        4. ability to deliver to maximum people (max units)
        5. does have shipping
        6. how much guarantee (if any)
        7. reviews of the business

        Each parameter is weighted with a certain percentage - specified in Constants class
        */
        public async void selectWinningBid(string groupId)
        {
            await DAL.BidDB.selectWinningBid(groupId);
        }

    }

}
