﻿using System;
using System.Web.Mvc;
using Parse;
using System.Collections.Generic;
using Facebook;
using System.Web.Security;
using System.Web;

namespace WebsiteMVC.Controllers
{
    public class UserAccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Login(FormCollection coll)
        {
            string returnUrl = coll["returnUrl"];
            string username = coll["username"];
            string password = coll["password"];

            Navigation.State.CurrentUser = await DAL.UserAccountDB.LoginUser(username, password);
            if(Navigation.State.CurrentUser == null) // failed to log in 
            {
                Navigation.State.CurrentUser = null;
                Models.UserModel user = new Models.UserModel();
                user.login = false;
                return View(user);
            }

            // login successfull
            return new RedirectResult(returnUrl);
        }


        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Register(FormCollection coll)
        {
            Models.UserModel user = new Models.UserModel();
            string Business = coll["isBusiness"];
            bool isBusiness = false;
            if (Business!=null && Business.Equals("True"))
                isBusiness = true;
            string firstName = coll["firstname"];
            string lastName = coll["lastname"];
            string returnUrl = coll["returnUrl"];
            string email = coll["email"];
            string userName = coll["username"];
            string password = coll["password"];
            string confirmPassword = coll["confirmpassword"];

            if (password.Equals(confirmPassword))
            {
                string tryRegister = await DAL.UserAccountDB.RegisterNewUser(userName, firstName, lastName, email, password, isBusiness);
                if (tryRegister.Equals("sucessfull"))
                {
                    BLL.User currentUser = await DAL.UserAccountDB.RegisterNewUser();
                    Navigation.State.CurrentUser = currentUser;
                    if (isBusiness)
                        return RedirectToAction("CompleteBusinessDetails", "Business"); //redirect to dashboard to complete business details page
                    return new RedirectResult(returnUrl);
                }
                else {
                    user.register = false;
                    user.registerError = tryRegister;
                    return View(user);
                }
            }
            else {
                user.register = false;
                user.registerError = "Passwords are not the same";
                return View(user);
            }   
        }
        
        
        public ActionResult LogOut(string returnUrl)
        {
            Parse.ParseUser.LogOut();
            HttpCookie getCurrentCookie = Request.Cookies[Constants.COOKIE_NAME];
            HttpContext.Response.Cookies.Remove(Constants.COOKIE_VALUES_USERID);
            HttpContext.Response.Cookies.Remove(Constants.COOKIE_VALUES_AUTH);
            getCurrentCookie.Expires = DateTime.Now.AddDays(-1);
            getCurrentCookie.Value = null;
            HttpContext.Response.SetCookie(getCurrentCookie);
            Navigation.State.CurrentUser = null;
            return new RedirectResult(returnUrl);
        }

        [AllowAnonymous]
        public ActionResult ChangePassword()
        {
            Models.UserModel user = new Models.UserModel();
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> ChangePassword(FormCollection coll)
        {
            string email = coll["email"];
            ParseObject reset = await DAL.UserAccountDB.ResetUserPassword(email);

            Models.UserModel user = new Models.UserModel();
            user.changePasswordMailSent = true;
            return View(user);
        }








        // ------ Facebook Login ------- // 
        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FacebookCallback");
                return uriBuilder.Uri;
            }
        }



        public async System.Threading.Tasks.Task<ActionResult> LoginWithFacebook()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = "578999652278537",
                client_secret = "0e92edc25ebde0bb206ae1d2dfeacd4b",
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email"
            });

            return Redirect(loginUrl.AbsoluteUri);

        }


        public ActionResult FacebookCallback(string code)
        {
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = "578999652278537",
                client_secret = "0e92edc25ebde0bb206ae1d2dfeacd4b",
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code
            });

            var accessToken = result.access_token;

            // Store the access token in the session for farther use
            Session["AccessToken"] = accessToken;

            // update the facebook client with the access token so
            // we can make requests on behalf of the user
            fb.AccessToken = accessToken;

            // Get the user's information, like email, first name, middle name etc
            dynamic me = fb.Get("me?fields=first_name,middle_name,last_name,id,email");
            string email = me.email;
            string firstname = me.first_name;
            string middlename = me.middle_name;
            string lastname = me.last_name;

            // Set the auth cookie
            FormsAuthentication.SetAuthCookie(email, false);
            return RedirectToAction("Index", "Home");
        }


    }
}