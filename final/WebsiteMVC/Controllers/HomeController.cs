﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebsiteMVC.Controllers
{
    public class HomeController : Controller
    {
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            HttpCookie getCookie = Request.Cookies[Constants.COOKIE_NAME];
            if (getCookie != null)
            {
                Navigation.State.CurrentUser = await DAL.UserAccountDB.ParseCurrentUserToBLL(getCookie.Values[Constants.COOKIE_VALUES_USERID], getCookie.Values[Constants.COOKIE_VALUES_AUTH]);
            }
            else
            {
                Navigation.State.CurrentUser = null;
            }

            BLL.HomePageModel homePageModel = new BLL.HomePageModel();
            homePageModel = await DAL.GroupBuyingDB.loadHomePageGroups(Navigation.State.CurrentUser);

            return View(homePageModel);
        }

        public async System.Threading.Tasks.Task<ActionResult> Search(FormCollection coll)
        {
            string productId = coll["search_ProductID"];
            Models.GroupBuyingModel m = new Models.GroupBuyingModel();
            m.group = await DAL.GroupBuyingDB.loadGroupByProduct(productId);
            if(m.group == null)
                return View("GroupNotFound");
            return RedirectToAction("Group", "Group", new { groupId = m.group.groupBuyingID, productTypeName = "product_cellphone" });
        }

        public ActionResult ErrorPage(string errorPage)
        {
            Models.ErrorModel m = new Models.ErrorModel();
            if (errorPage.Equals("Dashboard"))
            {
                m.errorMessage = "על מנת לצפות בפאנל ניהול העסק שלך יש להרשם לאתר כבעל עסק";
            }
            if (errorPage.Equals("Checkout"))
            {
                m.errorMessage = "על מנת להצטרף לקבוצת רכישה יש להרשם לאתר תחילה";
            }
            if (errorPage.Equals("CreateGroup"))
            {
                m.errorMessage = "על מנת ליצור קבוצה חדשה יש צורך להרשם לאתר";
            }
            if (errorPage.Equals("CreateBidLink"))
            {
                m.errorMessage = "על מנת להגיש הצעה יש להתחבר כבעל עסק";
            }
            if (errorPage.Equals("CreateGroupBusiness"))
            {
                m.errorMessage = "בעל עסק לא יכול לפתוח קבוצה באתר, יש להרשם כלקוח";

            }

            return View(m);
        }




        
        public async Task<JsonResult> SearchAutocomplete(string term)
        {
            List<BLL.Product> products = await DAL.ProductDB.SearchAutocomplete(term);
            var model = products.Select(p => new
                        {
                                    label = p.title.Trim(),
                                    id = p.productID.Trim()            
                        });
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}