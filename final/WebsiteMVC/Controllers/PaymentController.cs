﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebsiteMVC.Controllers
{
    public class PaymentController : Controller
    {
        private static OAuthTokenCredential tokenCredential = null;
        private static readonly object padlock = new object();
        private const int TOKEN_TIMEOUT_GAP = 300; 

        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }

        //thread safe access token generation (double checking)
        private string getAccessToken()
        {
            if (tokenCredential == null || tokenCredential.AccessTokenExpirationInSeconds < TOKEN_TIMEOUT_GAP)
            {
                lock (padlock)
                {
                    if (tokenCredential == null)
                    {
                        var config = ConfigManager.Instance.GetProperties();
                        tokenCredential = new OAuthTokenCredential(config);
                    }
                }
            }
            return tokenCredential.GetAccessToken();
        }

        /*
        Main method. flow:
        1. gets aaccess token. if token is expire, get a new one.
        2. get credit card ID for this user. if he doesn't have one, create a new and save it on Parse
        3. make payment
        */
        private void doCheckOut(string cardType, string cardNumber, int expireMonth, int expireYear, string firstName, string lastName, double sumOfMoney, string transactionDetails)
        {
            string accessToken = getAccessToken();
            APIContext apiContext = getApiContext(accessToken);

            //cehck if this user already has credit card ID sored in our DB
            string creditCardId = DAL.UserParse.getCreditCatdId();
            if (creditCardId == null)
            {
                //TODO: check maybe .equals to ""
                //zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
                creditCardId = StoreCreditCardDetails(apiContext, accessToken, cardType, cardNumber, expireMonth, expireYear, firstName, lastName);
            }

            doPayment(apiContext, creditCardId, sumOfMoney, transactionDetails);
        }

        private APIContext getApiContext(string accessToken)
        {
            var apiContext = new APIContext(accessToken);
            apiContext.Config = ConfigManager.Instance.GetProperties();
            return apiContext;
        }

        /*
        create credit catd, save details on paypal and parse.
        The method is Synchronize on purpose, in order to return credit card ID for doCheckOut
        */
        private string StoreCreditCardDetails(APIContext apiContext, string accessToken, string cardType, string cardNumber, int expireMonth, int expireYear, string firstName, string lastName)
        {
            CreditCard credtCard = new CreditCard();
            credtCard.type = cardType;
            credtCard.number = cardNumber;
            credtCard.expire_month = expireMonth;
            credtCard.expire_year = expireYear;
            credtCard.first_name = firstName;
            credtCard.last_name = lastName;
            CreditCard createdCreditCard = credtCard.Create(apiContext);

            //save on parse
            DAL.UserParse.saveUserCreditCardId(createdCreditCard.id);

            return createdCreditCard.id;
        }


        private void doPayment(APIContext apiContext, string creditCardId, double sumOfMoney, string transactionDetails)
        {
            CreditCardToken credCardToken = new CreditCardToken();
            credCardToken.credit_card_id = creditCardId;

            FundingInstrument fundInstrument = new FundingInstrument();
            fundInstrument.credit_card_token = credCardToken;

            List<FundingInstrument> fundingInstrumentList = new List<FundingInstrument>();
            fundingInstrumentList.Add(fundInstrument);

            Payer payr = new Payer();
            payr.funding_instruments = fundingInstrumentList;
            payr.payment_method = "credit_card";

            Amount amnt = new Amount();
            amnt.currency = "INS";
            amnt.total = System.Convert.ToString(sumOfMoney);

            Transaction tran = new Transaction();
            tran.description = transactionDetails;
            tran.amount = amnt;

            List<Transaction> transactions = new List<Transaction>();
            transactions.Add(tran);

            Payment pymnt = new Payment();
            pymnt.intent = "sale";
            pymnt.payer = payr;
            pymnt.transactions = transactions;

            Payment createdPayment = pymnt.Create(apiContext);
        }
    }
}