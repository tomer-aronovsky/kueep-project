﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebsiteMVC.Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace WebsiteMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
