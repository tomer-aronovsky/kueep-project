﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteMVC.Models
{
    public class BusinessDashboardModel
    {
        public BLL.Business business { get; set; }
        public List<BLL.GroupBuying> myActiveGroups { get; set; }
        public List<BLL.GroupBuying> groupsWaitingForBids { get; set; }
        public AddProductToBusiness addProductToBusiness { get; set; }
        public List<BLL.Product> myProducts { get; set; }



        //in complete registration & settings pages
        public List<BLL.Product> chooseYourProducts { get; set; }

        public BusinessDashboardModel()
        {
            this.business = new BLL.Business();
            this.myActiveGroups = new List<BLL.GroupBuying>();
            this.groupsWaitingForBids = new List<BLL.GroupBuying>();
            this.addProductToBusiness = new AddProductToBusiness();
            this.myProducts = new List<BLL.Product>();
        }











        public class AddProductToBusiness
        {
            public List<BLL.Product> products { get; set; }
            public List<BLL.ProductCategory> productCategories { get; set; }
            public List<BLL.ProductSubCategory> productSubCategories { get; set; }


            public AddProductToBusiness()
            {
                this.products = new List<BLL.Product>();
                this.productCategories = new List<BLL.ProductCategory>();
                this.productSubCategories = new List<BLL.ProductSubCategory>();
            }
        }






    }
}