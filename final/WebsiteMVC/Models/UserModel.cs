﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteMVC.Models
{
    public class UserModel
    {
        public bool login { get; set; }
        public bool register { get; set; }
        public bool changePasswordMailSent { get; set; } // true if email sent ot reset the password
        public string registerError { get; set; }
        public BLL.User user { get; set; }


        public UserModel()
        {
            this.changePasswordMailSent = false;
        }


    }
}