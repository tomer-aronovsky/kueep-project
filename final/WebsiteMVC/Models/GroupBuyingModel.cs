﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteMVC.Models
{
    public class GroupBuyingModel
    {
        public BLL.GroupBuying group { get; set; }
        public BLL.Product product { get; set; }
        public BLL.Business business { get; set; }
        public bool groupCreatedNow { get; set; }
        public string userId { get; set; }
        public DateTime timeToOffer { get; set; }

        public BLL.SpecificProductDetails.ProductCellphone productCellphone { get; set; }




        public double totalPrice { get; set; }

    }
}