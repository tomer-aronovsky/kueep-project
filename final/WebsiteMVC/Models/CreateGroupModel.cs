﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteMVC.Models
{
    public class CreateGroupModel
    {
        public List<BLL.Product> products { get; set; }
        public List<BLL.ProductCategory> productCategories { get; set; }
        public List<BLL.ProductSubCategory> productSubCategories { get; set; }



    }
}