﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebsiteMVC.App_Start
{
    public class LoggingFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = (string)filterContext.RouteData.Values["action"];
            if(actionName.Equals("CreateBidLink") && (Navigation.State.CurrentUser == null || !Navigation.State.CurrentUser.isBusiness))
            {
                filterContext.Result = new RedirectResult("../Home/ErrorPage?errorPage=CreateBidLink");
                return;
            }
            if (actionName.Equals("Dashboard") && Navigation.State.CurrentUser == null)
            {
                filterContext.Result = new RedirectResult("../Home/ErrorPage?errorPage=Dashboard");
                return;
            }
            else if (actionName.Equals("Dashboard") && !Navigation.State.CurrentUser.isBusiness)
            {
                filterContext.Result = new RedirectResult("../Home/ErrorPage?errorPage=Dashboard");
                return;
            }
            else if (actionName.Equals("CreateGroup") && Navigation.State.CurrentUser == null)
            {
                filterContext.Result = new RedirectResult("../Home/ErrorPage?errorPage=CreateGroup");
                return;
            }
            else if (actionName.Equals("CreateGroup") && Navigation.State.CurrentUser.isBusiness)
            {
                filterContext.Result = new RedirectResult("../Home/ErrorPage?errorPage=CreateGroupBusiness");
                return;
            }
            else if (Navigation.State.CurrentUser == null && !actionName.Equals("Group"))
            {
                filterContext.Result = new RedirectResult("../Home/Index");
                return;
            }
            else
            {
                base.OnActionExecuting(filterContext);
            }
        }
    }
}