﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL
{
    public class HomePageModel
    {
        public List<miniProduct> products { get; set; }

        //constructor
        public HomePageModel()
        {
            products = new List<miniProduct>();
        }

        //add all data to products list
        public void add(string title, string imageUrl, string groupObjectId, DateTime groupExpitationDate,
            double camera, double storage, string productId)
        {
            products.Add(new miniProduct(title, imageUrl, groupObjectId, groupExpitationDate
                ,camera, storage, productId));
        }

        public void sortAccordingToPref(string userPref)
        {

            //add the score to each prodcut from the products list
            List<Pref> list = JsonConvert.DeserializeObject<List<Pref>>(userPref);
            double score = 0;
            string productId = null;
            foreach (Pref p in list)
            {
                //checks if there are duplicates
                if (productId == p.productId)
                    continue;
                score = p.score;
                productId = p.productId;
                miniProduct mp = products.Find(x => x.productId == productId);
                if (mp != null)
                {
                    mp.score = score;
                }
            }


            //sort the products list according to the scroe
            products.Sort((x, y) => y.score.CompareTo(x.score));
        }
    }




    public class miniProduct
    {
        public string title { get; set; }
        public string imageUrl { get; set; }
        public string groupObjectId { get; set; }
        public DateTime groupExpitationDate { get; set; }
        public double camera { get; set; }
        public double storage { get; set; }
        public string productId { get; set; }
        public double score { get; set;}

        public bool realMiniProduct { get; set; }

        public miniProduct()
        {

        }

        public miniProduct(string title, string imageUrl, string groupObjectId, DateTime groupExpitationDate, 
           double camera, double storage, string productId)
        {
            this.title = title;
            this.imageUrl = imageUrl;
            this.groupObjectId = groupObjectId;
            this.groupExpitationDate = groupExpitationDate;
            this.camera = camera;
            this.storage = storage;
            this.productId = productId;
            this.score = 0;
        }
    }

    public class Pref
    {
        public double score { get; set; }
        public string productId { get; set; }
    }
}