﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL.SpecificProductDetails
{
    public class ProductCellphone
    {
        public string productCellphoneID { get;set; }
        public double backCamera { get; set; }
        public double frontCamera { get; set; }
        public string operationSystem { get; set; }
        public string screenSize { get; set; }
        public string storageCapacity { get; set; }



    }
}