﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL
{
    public class Product
    {
        public string productID { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public string productTypeName { get; set; }
        public string sliderImage1 { get; set; }
        public string sliderImage2 { get; set; }
        public string sliderImage3 { get; set; }

        //pointers
        public Manufacturer manufacturer { get; set; }
        public SpecificProductDetails.ProductCellphone productCellphone { get; set; }
        public SpecificProductDetails.ProductTelevision productTelevision { get; set; }

        //relations
        public List<Business> business { get; set; } // list of business sells this product

        public Product()
        {
            this.manufacturer = new Manufacturer();
        }
    }
}