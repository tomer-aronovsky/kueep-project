﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL
{
    public class ProductCategory
    {
        public string productCategoryID { get; set; }
        public string name { get; set; }



        //pointer - sub category
        public List<ProductSubCategory> productSubCategory { get; set; }

    }
}