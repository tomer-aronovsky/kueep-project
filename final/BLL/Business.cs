﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL
{
    public class Business
    {
        public string businessID { get; set; }
        public string address { get; set; }
        public string facebookPage { get; set; }
        public HttpPostedFileBase logo { get; set; }
        public string businessLogo { get; set; }
        public string businessName { get; set; }
        public string phoneNumber { get; set; }
        public string website { get; set; }
        public string aboutUs { get; set; }


        //Relations 
        public List<BLL.Product> products { get; set; }
    }
}