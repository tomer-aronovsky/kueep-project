﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL
{
    public class GroupBuying
    {
        public string groupBuyingID { get; set; }
        public DateTime expirationDate { get; set; }
        public bool active { get; set; }

        //pointers & relation
        public BLL.Bid winningBid { get; set; }
        public BLL.Product product { get; set; }
        public List<BLL.Bid> bids { get; set; }
        public List<BLL.User> usersInGroup { get; set; }

        // extras
        public double totalPriceForNow { get; set; }



        public GroupBuying()
        {
            this.bids = new List<Bid>();
            this.product = new Product();
            this.usersInGroup = new List<User>();
            this.winningBid = new Bid();
        }
    }
}