﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL
{
    public class Bid
    {
        public string bidID { get; set; }
        public string comment { get; set; }
        public double  guarantee { get; set; }
        public double max_units { get; set; }
        public double original_price { get; set; }
        public double price_step_1 { get; set; }
        public double price_step_2 { get; set; }
        public double price_step_3 { get; set; }
        public double price_step_4 { get; set; }
        public double price_step_5 { get; set; }
        public bool shipping { get; set; }
        public double size_step_1 { get; set; }
        public double size_step_2 { get; set; }
        public double size_step_3 { get; set; }
        public double size_step_4 { get; set; }
        public double size_step_5 { get; set; }
        public string businessId { get; set; }



        public Bid()
        {
        }



    }
}