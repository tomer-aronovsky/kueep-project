﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL
{
    public class User
    {
        public string userID { get; set; }
        public string username { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public bool isClient { get; set; }
        public bool isBusiness { get; set; }
        public List<string> groupsIDs { get; set; } //groups that user member
        public string businessID { get; set; }



        public bool loginSuccessfull { get; set; }

        public User()
        {
            this.groupsIDs = new List<string>();
            this.loginSuccessfull = false;
            this.isBusiness = false;
            this.isClient = true;
        }

    }
}