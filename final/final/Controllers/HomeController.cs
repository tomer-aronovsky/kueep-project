﻿using ClientMVC.Models;
using Parse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClientMVC.Controllers
{
    public class HomeController : Controller
    {


        public async System.Threading.Tasks.Task<ActionResult> Index()
        {

            GroupBuyingModel m = new GroupBuyingModel();
            m.groups = await DAL.GroupBuyingDB.loadHomePageGroups();


            return View(m);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {

            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}